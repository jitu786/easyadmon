package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import e.admin.R;
import e.admin.generalfiles.GeneralFunctions;

public class Dashboard extends AppCompatActivity {

    ImageView navigation_menu;
    CardView collection_point_list,custom_order,manage_product,user_detail,agent,returnOrder;
    GeneralFunctions generalFunc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initialize();
    }
    public void initialize()
    {
        navigation_menu=findViewById(R.id.navigation_menu);
        collection_point_list=findViewById(R.id.collection_point_list);
        custom_order=findViewById(R.id.custom_order);
        manage_product=findViewById(R.id.manage_product);
        returnOrder=findViewById(R.id.returnOrder);
        user_detail=findViewById(R.id.user_detail);
        agent=findViewById(R.id.agent);
        generalFunc=new GeneralFunctions(Dashboard.this);
        collection_point_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), CollectionPoinList.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });
        manage_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Manage_Product.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });
        custom_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), CustomOrder.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });
        navigation_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i2 = new Intent(getApplicationContext(), NavigationMenu.class);
                startActivity(i2);
                overridePendingTransition( R.anim.slide_in_down, R.anim.slide_out_down );


            }
        });
        returnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i2 = new Intent(getApplicationContext(), ReturnOrder.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });
        user_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), UserDetails.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });
        agent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), AgentDetails.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );
            }
        });
    }
}
