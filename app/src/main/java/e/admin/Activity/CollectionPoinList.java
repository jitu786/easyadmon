package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.CollectionPointListAdapter;
import e.admin.Adapter.Message_Adapter;
import e.admin.R;

public class CollectionPoinList extends AppCompatActivity {
    TextView tool_txt;
    ImageView back;
    ImageView add_collection_point;
    RecyclerView collection_point_list;
    CollectionPointListAdapter adapter;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_poin_list);
        initialize();

    }

    public void initialize()
    {
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        add_collection_point=findViewById(R.id.add);
        collection_point_list=findViewById(R.id.collection_point_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("Collection Points");


        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(CollectionPoinList.this, LinearLayoutManager.VERTICAL, false);
        collection_point_list.setLayoutManager(layoutManager);
        adapter = new CollectionPointListAdapter(this);
        collection_point_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        //getMessages();

        add_collection_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AddCollectionPoint.class));
            }
        });
    }
}
