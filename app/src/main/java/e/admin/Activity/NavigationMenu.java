package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.R;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.utils.SharedPref;

public class NavigationMenu extends AppCompatActivity {

    ImageView cross;
    CardView my_message,assigned,proceed,change_password,logout;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_menu);

        initialize();
    }
    public void initialize()
    {
        generalFunc=new GeneralFunctions(NavigationMenu.this);
        cross=findViewById(R.id.cross);
        my_message=findViewById(R.id.my_message);
        assigned=findViewById(R.id.assigned);
        proceed=findViewById(R.id.proceed);
        change_password=findViewById(R.id.change_password);
        logout=findViewById(R.id.logout);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
            }
        });

        my_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), My_Message.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );


            }
        });

        assigned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Assigned.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );


            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(getApplicationContext(), Proceed.class);
                startActivity(i2);
                overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle=new Bundle();
                bundle.putString("codeVerification","codeVerification");
                bundle.putString("flag","account");
                bundle.putString("userId",generalFunc.getMemberId());
                generalFunc.startActivity(ChangePassword.class,bundle);
               /* Intent i2 = new Intent(getApplicationContext(), ChangePassword.class);
                startActivity(i2);
               */ overridePendingTransition( R.anim.left_to_right, R.anim.right_to_left );

            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logout_user();

            }
        });
    }
    private void logout_user() {
        SharedPref.ClearaSharePrefrence(NavigationMenu.this);
        SharedPref.WriteSharePrefrence1(this,"newarrival",dataList);
        SharedPref.WriteSharePrefrence1(this,"trading",dataList);
        SharedPref.WriteSharePrefrence1(this,"userCode",dataList);

        Intent intent=new Intent(this, LogIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
