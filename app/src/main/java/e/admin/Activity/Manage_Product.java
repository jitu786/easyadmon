package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.Manage_Product_Adapter;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

import static e.admin.generalfiles.Utils.closeLoader;

public class Manage_Product extends AppCompatActivity /*implements Manage_Product_Adapter.OnItemClickListener*/ {





    RecyclerView store_product_list;
    Manage_Product_Adapter adapter;
    ImageView add,imgback;
    View sp_tool;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ImageView cancle_search;
    RelativeLayout search_layout;
    EditText editTextSearch;
    ImageView search;
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage__product);
        initialize();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Add_Product.class));
            }
        });


       /* imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/


    }
    public void initialize()
    {
        imgback=findViewById(R.id.imgback);
        sp_tool=findViewById(R.id.sp_tool);
        back=findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        store_product_list=findViewById(R.id.store_product_list);
        add=findViewById(R.id.add);
        generalFunc=new GeneralFunctions(this);

        search_layout=findViewById(R.id.search_layout);
        editTextSearch=findViewById(R.id.editTextSearch);
        cancle_search=findViewById(R.id.cancle_search);
        search=findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search.setVisibility(View.GONE);
                search_layout.setVisibility(View.VISIBLE);
                add.setVisibility(View.GONE);
            }
        });
        cancle_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_layout.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);
                add.setVisibility(View.VISIBLE);
            }
        });


        @SuppressLint("WrongConstant") GridLayoutManager layoutManager_seller=new GridLayoutManager(Manage_Product.this,2, GridLayoutManager.VERTICAL,false);
        store_product_list.setLayoutManager(layoutManager_seller);
        adapter = new Manage_Product_Adapter(this,dataList);
        store_product_list.setAdapter(adapter);
        store_product_list.setNestedScrollingEnabled(false);
        editTextSearch.addTextChangedListener(generalTextWatcher);
        //adapter.setOnItemClickListener(this);
        adapter.notifyDataSetChanged();
        findProducts();


    }
    public void findProducts()
    {
        dataList.clear();
        adapter.notifyDataSetChanged();


        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getListOfStoreProducts");
        parameters.put("customer_id",generalFunc.getMemberId());
        parameters.put("page_index", "1");

        Utils.printLog("ProductsCategoryParameters", "::" + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setIsDeviceTokenGenerate(true, "vDeviceToken");
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals(""))
                {
                    //load_more=false;
                    //adapter.removeFooterView();
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    //      dataList.clear();
                    //   adapter.notifyDataSetChanged();

                    if (isDataAvail == true)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null)
                        {

                            for (int i = 0; i < msgArr.length(); i++)
                            {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                String productImg = generalFunc.getJsonValue("image", obj_temp);
                                String productName = generalFunc.getJsonValue("name", obj_temp);
                                String productDes = generalFunc.getJsonValue("description", obj_temp);
                                String productId = generalFunc.getJsonValue("product_id", obj_temp);
                                String price = generalFunc.getJsonValue("price", obj_temp);
                                String catId = generalFunc.getJsonValue("category_id", obj_temp);
                                String product_type = generalFunc.getJsonValue("product_type", obj_temp);
                                String status = generalFunc.getJsonValue("status", obj_temp);

                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("status", status);
                                dataMap_products.put("product_type", product_type);
                                dataMap_products.put("name", productName);
                                dataMap_products.put("category_id", catId);
                                dataMap_products.put("product_id", productId);
                                dataMap_products.put("price", price);
                                dataMap_products.put("description", Utils.html2text(productDes));
                                dataMap_products.put("image", productImg);
                                // dataMap_products.put("TYPE", "" + StoreProductsRecyclerAdapter.TYPE_ITEM);
                                dataList.add(dataMap_products);
                            }
                            adapter.notifyDataSetChanged();

                            /*currentPage=currentPage+1;
                            load_more=true;
                            rightImgView.setVisibility(View.VISIBLE);
                            listChangeImgView.setVisibility(View.VISIBLE);
*/
                        }
                        // adapter.notifyDataSetChanged();

                    } else
                    {
                        //  noProductsTxt.setText(generalFunc.getJsonValue(Utils.message_str, responseString));
                        // noDataArea.setVisibility(View.VISIBLE);
                        adapter.notifyDataSetChanged();

                    }

                    closeLoader();
                } else {
                    adapter.notifyDataSetChanged();
                    //generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }

    /*
        @Override
        public void onItemClickList(View v, int position, int btnType, int qty) {
            if(btnType==1)
            {
                deleteStoreProduct(dataList.get(position).get("product_id"));
            }
            else if(btnType==2)
            {
                Intent intent=new Intent(getActContext(),Add_Product.class);
                intent.putExtra("product_id",dataList.get(position).get("product_id"));
                startActivity(intent);
            }
        }*/
    public void deleteStoreProduct(String product_id) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "deleteStoreProduct");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", product_id);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        findProducts();

                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private TextWatcher generalTextWatcher = new TextWatcher()
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            filter(s.toString());


        }

    };



    private Context getActContext() {
        return Manage_Product.this;
    }
    private void filter(String text) {
//new array list that will hold the filtered data
//looping through existing elements
        ArrayList<HashMap<String, String>> dataList1 = new ArrayList<>();
        int k=0;

        for (int i=0;i<dataList.size();i++) {
            HashMap<String,String> map=dataList.get(i);

            if (map.get("name").toLowerCase().contains(text.toLowerCase())||
                    map.get("product_id").toLowerCase().contains(text.toLowerCase()))
            {
                dataList1.add(map);
            }
        }
        adapter.filterList(dataList1);
    }
}
