package e.admin.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.CustomOrderAdapter;
import e.admin.Adapter.CustomReturnOrder;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

public class ReturnOrder extends AppCompatActivity {

    ImageView add;
    RecyclerView custom_order_list;
    CustomReturnOrder adapter;
    TextView tool_txt;
    ImageView back;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ImageView cancle_search;
    RelativeLayout search_layout;
    EditText editTextSearch;
    ImageView search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_order_details);
        initialize();
    }

    private void initialize() {


        generalFunc=new GeneralFunctions(ReturnOrder.this);

        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        custom_order_list=findViewById(R.id.custom__return_order_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("Custom Return Order");
        search_layout=findViewById(R.id.search_layout);
        editTextSearch=findViewById(R.id.editTextSearch);
        cancle_search=findViewById(R.id.cancle_search);
        search=findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search.setVisibility(View.GONE);
                search_layout.setVisibility(View.VISIBLE);
                tool_txt.setVisibility(View.GONE);
            }
        });
        cancle_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_layout.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);
                tool_txt.setVisibility(View.VISIBLE);
            }
        });



        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(ReturnOrder.this, LinearLayoutManager.VERTICAL, false);
        custom_order_list.setLayoutManager(layoutManager);
        adapter = new CustomReturnOrder(this,dataList);
        //adapter.setOnItemClickListener(this);
        custom_order_list.setAdapter(adapter);
        editTextSearch.addTextChangedListener(generalTextWatcher);
        custom_order_list.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();
       getreturnOrder();

    }

    private void getreturnOrder() {

        dataList.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getReturnOrder");

        Log.e("param",parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ReturnOrder.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals(""))
                {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        dataList.clear();
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                Log.d("data",msgArr.toString());
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("order_id",generalFunc.getJsonValue("order_id", String.valueOf(obj_service)));
                                dataMap_products.put("return_id",generalFunc.getJsonValue("return_id", String.valueOf(obj_service)));
                                dataMap_products.put("product_name",generalFunc.getJsonValue("product_name", String.valueOf(obj_service)));
                                dataMap_products.put("customer_id",generalFunc.getJsonValue("customer_id", String.valueOf(obj_service)));
                                dataMap_products.put("firstname",generalFunc.getJsonValue("firstname", String.valueOf(obj_service)));
                                dataMap_products.put("lastname",generalFunc.getJsonValue("lastname", String.valueOf(obj_service)));
                                dataMap_products.put("email",generalFunc.getJsonValue("email", String.valueOf(obj_service)));
                                dataMap_products.put("telephone",generalFunc.getJsonValue("telephone", String.valueOf(obj_service)));
                                dataMap_products.put("reason_name",generalFunc.getJsonValue("reason_name", String.valueOf(obj_service)));
                                dataMap_products.put("return_status_name",generalFunc.getJsonValue("return_status_name", String.valueOf(obj_service)));
                                dataMap_products.put("action_name",generalFunc.getJsonValue("action_name", String.valueOf(obj_service)));
                                dataMap_products.put("return_action_id",generalFunc.getJsonValue("return_action_id", String.valueOf(obj_service)));
                                dataMap_products.put("return_status_id",generalFunc.getJsonValue("return_status_id", String.valueOf(obj_service)));
                                dataMap_products.put("product_id",generalFunc.getJsonValue("product_id", String.valueOf(obj_service)));

                                dataList.add(dataMap_products);

                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                    else {
                        adapter.notifyDataSetChanged();
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }

    private TextWatcher generalTextWatcher = new TextWatcher()
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            filter(s.toString());


        }

    };



    private void filter(String text)
    {
        ArrayList<HashMap<String, String>> dataList1 = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++)
        {
            HashMap<String, String> dataMap_products=dataList.get(i);
            if(dataMap_products.get("order_id").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("product_name").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("return_status_name").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("return_id").toUpperCase().contains(text.toUpperCase()))
            {
                dataList1.add(dataMap_products);
            }
        }
        adapter.filterList(dataList1);
    }

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(ReturnOrder.this,Dashboard.class);
        startActivity(intent);
        finish();
    }
}
