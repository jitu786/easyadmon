package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.AgentDetailsAdapter;
import e.admin.Adapter.UserDetailAdapter;
import e.admin.R;

public class AgentDetails extends AppCompatActivity {

    TextView tool_txt;
    ImageView back;
    ImageView add;
    RecyclerView agent_data_list;
    AgentDetailsAdapter adapter;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_details);
        initialize();

    }

    public void initialize()
    {
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        agent_data_list=findViewById(R.id.agent_data_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("Agent Detail");


        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(AgentDetails.this, LinearLayoutManager.VERTICAL, false);
        agent_data_list.setLayoutManager(layoutManager);
        adapter = new AgentDetailsAdapter(this);
        agent_data_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AddAgent.class));
            }
        });

    }
}
