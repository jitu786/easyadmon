package e.admin.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

public class ChangeReturnOrderStatus extends AppCompatActivity {

    TextView tool_txt, done;
    ImageView back;
    //  AdapterChangeStatus adapter;
    RecyclerView status_list;
    ArrayList<HashMap<String, String>> dataList1 = new ArrayList<>();
    List<String> status_id_list = new ArrayList<>();
    GeneralFunctions generalFunc;
    Spinner spinner_status;
    String Order_status_id = "";
    ArrayList<String> status_array = new ArrayList<>();
    ArrayAdapter aa;
    HashMap<String, String> OrderData = null;
    TextView order_id,return_id, p_name, u_name, email, telephone, reson_name, status,action_txt;

    String return_action_id;
    RelativeLayout radioview_data;
    RadioGroup radiogroup;
    RadioButton refunded,creditissue,replacementsend;
    String action_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_return);

        initialize();
    }

    private void initialize() {


        generalFunc=new GeneralFunctions(ChangeReturnOrderStatus.this);
        done=findViewById(R.id.done);
        order_id=findViewById(R.id.order_id);
        return_id=findViewById(R.id.return_id);
        p_name=findViewById(R.id.p_name);
        u_name=findViewById(R.id.u_name);
        email=findViewById(R.id.email);
        telephone=findViewById(R.id.telephone);
        status=findViewById(R.id.status);
        reson_name=findViewById(R.id.reson_name);
        action_txt=findViewById(R.id.action_txt);
        radioview_data=findViewById(R.id.radioview_data);
        radiogroup=findViewById(R.id.radiogroup);
        refunded=findViewById(R.id.refunded);
        creditissue=findViewById(R.id.creditissue);
        replacementsend=findViewById(R.id.replacementsend);

        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        tool_txt.setText("Change Order Return Status");


        OrderData = (HashMap<String, String>) getIntent().getSerializableExtra("oderDATA");


        setData();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!action_id.isEmpty()) {

                    updatestatus();
                }
                else {
                    Toast.makeText(ChangeReturnOrderStatus.this, "Please Select Any One Action", Toast.LENGTH_SHORT).show();
                }//startActivity(new Intent(getApplicationContext(), CustomOrder.class));
            }
        });


        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch(checkedId){
                    case R.id.refunded:

                        action_id="1";
                        Order_status_id="11";
                        // do operations specific to this selection
                        break;
                    case R.id.creditissue:
                        action_id="2";
                        Order_status_id="13";
                        break;
                    case R.id.replacementsend:
                        action_id="3";
                        Order_status_id="5";
                        break;
                }
            }
        });


    }

    private void updatestatus() {


        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "updateActionStatus");
        parameters.put("return_action_id",action_id);
        parameters.put("return_id",OrderData.get("return_id"));


        Log.d("parater",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ChangeReturnOrderStatus.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString)
            {
                if (responseString != null && !responseString.equals("")) {


                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        updateorderstatus();
                       /* Toast.makeText(ChangeReturnOrderStatus.this, generalFunc.getJsonValue("message",responseString), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ChangeReturnOrderStatus.this,ReturnOrder.class);
                        startActivity(intent);
                        finish();*/
                        // Toast.makeText(EditArticleList.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ChangeReturnOrderStatus.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        exeWebServer.execute();
    }
    private void updateorderstatus() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "updateOrderStatus");
        parameters.put("order_status_id",Order_status_id);
        parameters.put("order_id",OrderData.get("order_id"));
        parameters.put("product_id",OrderData.get("product_id"));
        Log.d("parater",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ChangeReturnOrderStatus.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString)
            {
                if (responseString != null && !responseString.equals("")) {


                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        Toast.makeText(ChangeReturnOrderStatus.this, "Your status is updated sucessfully.", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ChangeReturnOrderStatus.this,ReturnOrder.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(ChangeReturnOrderStatus.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        exeWebServer.execute();


    }

    private void setData() {


        if (OrderData != null) {
            order_id.setText(OrderData.get("order_id"));
            return_id.setText(OrderData.get("return_id"));
            p_name.setText(OrderData.get("product_name"));
            u_name.setText(OrderData.get("firstname")+" "+OrderData.get("lastname"));
            email.setText(OrderData.get("email"));
            telephone.setText(OrderData.get("telephone"));
            status.setText(OrderData.get("return_status_name"));
            reson_name.setText(OrderData.get("reason_name"));
            return_action_id=OrderData.get("return_action_id");

            if (OrderData.get("action_name").equals("") && OrderData.get("action_name").isEmpty())
            {
                action_txt.setText("Waiting");
            }
            else {
                action_txt.setText(OrderData.get("action_name"));
            }

            if (return_action_id.equals("0"))
            {
                radioview_data.setVisibility(View.VISIBLE);
            }
            else
            {
                radioview_data.setVisibility(View.GONE);
                done.setVisibility(View.GONE);
            }
        }

    }
}
