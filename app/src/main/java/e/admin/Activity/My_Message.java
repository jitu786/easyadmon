package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.Message_Adapter;
import e.admin.R;

public class My_Message extends AppCompatActivity {

    RecyclerView message_list;
    TextView tool_txt;
    ImageView back;
    Message_Adapter adapter;
    //GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__message);

        initialize();
    }
    public void initialize()
    {
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        message_list=findViewById(R.id.message_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("My Message");


        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(My_Message.this, LinearLayoutManager.VERTICAL, false);
        message_list.setLayoutManager(layoutManager);
        adapter = new Message_Adapter(this);
        message_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        //getMessages();
    }

  /*  public void getMessages()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getMessages");
        //parameters.put("customer_id","11");
        parameters.put("customer_id",generalFunc.getMemberId());
        Log.e("cust_id",""+generalFunc.getMemberId());



        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(My_Message.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        dataList.clear();
                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("message_id",generalFunc.getJsonValue("message_id", String.valueOf(obj_service)));
                                dataMap_products.put("customer_id",generalFunc.getJsonValue("customer_id", String.valueOf(obj_service)));
                                dataMap_products.put("title",generalFunc.getJsonValue("title", String.valueOf(obj_service)));
                                dataMap_products.put("message",generalFunc.getJsonValue("message", String.valueOf(obj_service)));
                                dataMap_products.put("date",generalFunc.getJsonValue("date", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }
                            adapter.notifyDataSetChanged();

                        }
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();


    }*/
}
