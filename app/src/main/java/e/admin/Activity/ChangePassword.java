package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.view.GenerateAlertBox;

public class ChangePassword extends AppCompatActivity {

    ImageView pd_back;
    TextView po_title;
    GeneralFunctions generalFunc;
    Button reset_password;
    String otpcode="",customer_id="";
    TextView new_show,confirm_show;

    EditText otp,new_password,confirm_password;
    private static boolean isSocialLogin,account=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initialize();
        reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (chekvalidation())
                {
                    updatePassword();
                }


            }
        });
        pd_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        new_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPassword(new_password,new_show);

            }
        });

        confirm_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPassword(confirm_password,confirm_show);
            }
        });


    }

    private void updatePassword() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "changePassword");

        if(isSocialLogin){
            parameters.put("codeVerification", "codeVerification");
            parameters.put("customer_id", getIntent().getStringExtra("userId"));
        }else {
            parameters.put("currentPassword", Utils.getText(otp));
            parameters.put("customer_id", generalFunc.getMemberId());
        }

        parameters.put("newPassword", Utils.getText(new_password));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {

                Utils.printLog("ResponseData", "Data::" + responseString);

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail)
                    {
                        if(account==false)
                        {
                            final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                            generateAlert.setContentMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                            generateAlert.setPositiveBtn("Ok");
                            generateAlert.showAlertBox();
                            generateAlert.setCancelable(false);

                            generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                                @Override
                                public void handleBtnClick(int btn_id) {
                                    if (btn_id == 1) {
                                        if (isSocialLogin) {
                                            generalFunc.storeUserData(getIntent().getStringExtra("userId"));
                                            generalFunc.getConfirmDialog().setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                                                @Override
                                                public void handleBtnClick(int btn_id) {
                                                    /*Intent intent = new Intent(getActContext(), Shipping_Address.class);

                                                    if (btn_id == 1) {
                                                        intent.putExtra("isSeller", true);
                                                    } else {
                                                        intent.putExtra("isSeller", false);
                                                    }

                                                    intent.putExtra("registerUser", true);
                                                    intent.putExtra("firstName", getIntent().getStringExtra("firstName"));
                                                    intent.putExtra("lastName", getIntent().getStringExtra("lastName"));
                                                    startActivity(intent);
                                                    finish();*/

                                                    Toast.makeText(ChangePassword.this, "done", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else {
// backImgView.performClick();
                                        }

                                    }
                                }
                            });
                        }
                        else
                        {
                            GenerateAlertBox alertBox= generalFunc.showGeneralMessage("",generalFunc.getJsonValue(Utils.message_str, responseString));
                            alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                                @Override
                                public void handleBtnClick(int btn_id) {
                                    Intent intent=new Intent(ChangePassword.this, Dashboard.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });

                        }

                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext() {

        return ChangePassword.this;
    }


    private boolean chekvalidation() {

        if(isSocialLogin){
            if (new_password.getText().toString().isEmpty()) {
                new_password.setError("Password is Required");
                return false;
            }
            if (confirm_password.getText().toString().isEmpty()) {
                confirm_password.setError("ConfirmPassword is Required");
                return false;
            } else if (!new_password.getText().toString().trim().isEmpty() && !new_password.getText().toString().trim().equals(confirm_password.getText().toString().trim())) {
                confirm_password.setError("Password and confirm password should be same");
                return false;
            }
            return true;
        }
        else {


            if (otp.getText().toString().isEmpty()) {
                otp.setError("OTP Is Required");
                return false;
            } else if (!otp.getText().toString().equals(otpcode)) {
                otp.setError("OTP Does Not Match");
                return false;
            }

            if (new_password.getText().toString().isEmpty()) {
                new_password.setError("Password is Required");
                return false;
            }
            if (confirm_password.getText().toString().isEmpty()) {
                confirm_password.setError("ConfirmPassword is Required");
                return false;
            } else if (!new_password.getText().toString().trim().isEmpty() && !new_password.getText().toString().trim().equals(confirm_password.getText().toString().trim())) {
                confirm_password.setError("Password and confirm password should be same");
                return false;
            }
        }
        return true;
    }

    public void initialize()
    {
        po_title=findViewById(R.id.po_title);
        isSocialLogin=getIntent().hasExtra("codeVerification");
        pd_back=findViewById(R.id.pd_back);
        po_title.setText("Change Your Password");
        generalFunc=new GeneralFunctions(ChangePassword.this);
        reset_password=findViewById(R.id.reset_password);
        otpcode=getIntent().getStringExtra("OTP");
        otp=findViewById(R.id.otp);
        new_show=findViewById(R.id.new_show);
        confirm_show=findViewById(R.id.confirm_show);
        new_password=findViewById(R.id.new_password);
        confirm_password=findViewById(R.id.confirm_password);
        if(getIntent().getStringExtra("flag")!=null)
        {
            account=true;
        }



        if(isSocialLogin){
//po_title ="Save Password";
            otp.setVisibility(View.GONE);
        }

    }


    private void showPassword(EditText editText, TextView textView) {

        if(textView.getText().toString().equals("Show"))
        {
            textView.setText("Hide");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            new_password.setTypeface(type);
            confirm_show.setTypeface(type);
            editText.setInputType(InputType.TYPE_CLASS_TEXT );
            editText.setTypeface(type);



        }
        else
        {
            textView.setText("Show");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            new_password.setTypeface(type);
            confirm_show.setTypeface(type);
            editText.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText.setTypeface(type);

//editText.setInputType(InputType.TYPE_CLASS_TEXT );//| InputType.TYPE_TEXT_VARIATION_PASSWORD


        }

    }



}