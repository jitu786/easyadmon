
package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.CustomOrderAdapter;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

public class CustomOrder extends AppCompatActivity  /*implements  CustomOrderAdapter.OnItemClickListener*/{

    RecyclerView custom_order_list;
    CustomOrderAdapter adapter;
    TextView tool_txt;
    ImageView back;
    ImageView add;
    GeneralFunctions generalFunc;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ImageView cancle_search;
    RelativeLayout search_layout;
    EditText editTextSearch;
    ImageView search;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cutom_order);
        initialize();

    }


    public void initialize()
    {
        generalFunc=new GeneralFunctions(CustomOrder.this);
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        custom_order_list=findViewById(R.id.custom_order_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("Custom Order");
        search_layout=findViewById(R.id.search_layout);
        editTextSearch=findViewById(R.id.editTextSearch);
        cancle_search=findViewById(R.id.cancle_search);
        search=findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search.setVisibility(View.GONE);
                search_layout.setVisibility(View.VISIBLE);
                tool_txt.setVisibility(View.GONE);
            }
        });
        cancle_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_layout.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);
                tool_txt.setVisibility(View.VISIBLE);
            }
        });



        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(CustomOrder.this, LinearLayoutManager.VERTICAL, false);
        custom_order_list.setLayoutManager(layoutManager);
        adapter = new CustomOrderAdapter(this,dataList);
        //adapter.setOnItemClickListener(this);
        custom_order_list.setAdapter(adapter);
        editTextSearch.addTextChangedListener(generalTextWatcher);
        custom_order_list.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();
        getOrder();

    }

    private void getOrder() {


        dataList.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getCustomOrder");

        Log.e("param",parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(CustomOrder.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals(""))
                {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        dataList.clear();
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                Log.d("data",msgArr.toString());
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("order_id",generalFunc.getJsonValue("order_id", String.valueOf(obj_service)));
                                dataMap_products.put("invoice_no",generalFunc.getJsonValue("invoice_no", String.valueOf(obj_service)));
                                dataMap_products.put("invoice_prefix",generalFunc.getJsonValue("invoice_prefix", String.valueOf(obj_service)));
                                dataMap_products.put("store_id",generalFunc.getJsonValue("store_id", String.valueOf(obj_service)));
                                dataMap_products.put("store_name",generalFunc.getJsonValue("store_name", String.valueOf(obj_service)));
                                dataMap_products.put("store_url",generalFunc.getJsonValue("store_url", String.valueOf(obj_service)));
                                dataMap_products.put("customer_id",generalFunc.getJsonValue("customer_id", String.valueOf(obj_service)));
                                dataMap_products.put("customer_group_id",generalFunc.getJsonValue("customer_group_id", String.valueOf(obj_service)));
                                dataMap_products.put("firstname",generalFunc.getJsonValue("firstname", String.valueOf(obj_service)));
                                dataMap_products.put("lastname",generalFunc.getJsonValue("lastname", String.valueOf(obj_service)));
                                dataMap_products.put("email",generalFunc.getJsonValue("email", String.valueOf(obj_service)));
                                dataMap_products.put("telephone",generalFunc.getJsonValue("telephone", String.valueOf(obj_service)));
                                dataMap_products.put("status_name",generalFunc.getJsonValue("status_name", String.valueOf(obj_service)));
                                dataMap_products.put("product_name",generalFunc.getJsonValue("product_name", String.valueOf(obj_service)));
                                dataMap_products.put("total",generalFunc.getJsonValue("total", String.valueOf(obj_service)));
                                dataMap_products.put("payment_method",generalFunc.getJsonValue("payment_method", String.valueOf(obj_service)));
                                dataMap_products.put("payment_city",generalFunc.getJsonValue("payment_city", String.valueOf(obj_service)));
                                dataMap_products.put("payment_country",generalFunc.getJsonValue("payment_country", String.valueOf(obj_service)));
                                dataMap_products.put("shipping_city",generalFunc.getJsonValue("shipping_city", String.valueOf(obj_service)));
                                dataMap_products.put("order_status_id",generalFunc.getJsonValue("order_status_id", String.valueOf(obj_service)));
                                dataMap_products.put("product_id",generalFunc.getJsonValue("product_id", String.valueOf(obj_service)));


                                dataList.add(dataMap_products);
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                    else {
                        adapter.notifyDataSetChanged();
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    /* @Override
     public void onItemClickList(View v, int position, int btnType) {


         if(btnType==1)
         {
             Intent int_edit_address = new Intent(CustomOrder.this, ChangeOrderStatus.class);
             int_edit_address.putExtra("oderDATA", dataList.get(position));
             startActivity(int_edit_address);

         }
     }
 */
    private TextWatcher generalTextWatcher = new TextWatcher()
    {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            filter(s.toString());


        }

    };


    private void filter(String text)
    {
        ArrayList<HashMap<String, String>> dataList1 = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++)
        {
            HashMap<String, String> dataMap_products=dataList.get(i);
            if(dataMap_products.get("order_id").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("product_name").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("status_name").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("firstname").toUpperCase().contains(text.toUpperCase()) ||
                    dataMap_products.get("lastname").toUpperCase().contains(text.toUpperCase()))
            {
                dataList1.add(dataMap_products);
            }
        }
        adapter.filterList(dataList1);
    }


}