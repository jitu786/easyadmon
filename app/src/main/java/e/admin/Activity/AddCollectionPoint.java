package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import e.admin.Adapter.Message_Adapter;
import e.admin.Adapter.SelectCollectionPointAdapter;
import e.admin.R;

public class AddCollectionPoint extends AppCompatActivity {

    EditText select_collection_point;
    TextView tool_txt;
    ImageView back;
    SelectCollectionPointAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_collection_point);
        initialize();


    }

    public void initialize()
    {
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("Add Collection Point");
        select_collection_point=findViewById(R.id.select_collection_point);
select_collection_point.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog dialog1 = new Dialog(AddCollectionPoint.this);
        RecyclerView select_collection_point_list;
        dialog1.setContentView(R.layout.select_collection_point_dialog);
        Window window1 = dialog1.getWindow();
        WindowManager.LayoutParams wlp1 = window1.getAttributes();
        window1.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        wlp1.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp1.gravity = Gravity.CENTER_VERTICAL;
        window1.setAttributes(wlp1);
        select_collection_point_list=dialog1.findViewById(R.id.select_collection_point_list);
        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(AddCollectionPoint.this, LinearLayoutManager.VERTICAL, false);
        select_collection_point_list.setLayoutManager(layoutManager);
        adapter = new SelectCollectionPointAdapter(AddCollectionPoint.this);
        select_collection_point_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        dialog1.show();
    }
});



    }

}
