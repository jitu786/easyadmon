package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import e.admin.Fragment.Data_Form;
import e.admin.Fragment.Discount_Form;
import e.admin.Fragment.General_Form;
import e.admin.Fragment.Image_Form;
import e.admin.Fragment.Link_Form;
import e.admin.R;
import e.admin.generalfiles.GeneralFunctions;


public class Add_Product extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    public String product_id="";
    LinearLayout general,image,data,link,discount;
    FrameLayout add_product_container;
    ImageView linkimg,imgback;
    TextView linktxt;
    public GeneralFunctions generalFunc;
    TextView txtfinish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__product);
        initialize();
    }

    public void initialize()
    {
        txtfinish=findViewById(R.id.txtfinish);
        txtfinish.setVisibility(View.GONE);
        imgback=findViewById(R.id.imgback);
        generalFunc=new GeneralFunctions(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.add_product_container, new General_Form()).commit();
        link=findViewById(R.id.link);
        image=findViewById(R.id.image);
        data=findViewById(R.id.data);
        discount=findViewById(R.id.discount);
        general=findViewById(R.id.general);

        if(getIntent().getStringExtra("product_id")!=null)
            product_id=getIntent().getStringExtra("product_id");

        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtfinish.setVisibility(View.GONE);
                getSupportFragmentManager().beginTransaction().replace(R.id.add_product_container, new General_Form()).commit();

            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!product_id.isEmpty())
                {
                    txtfinish.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.add_product_container, new Image_Form()).commit();


                }
                else
                    Toast.makeText(Add_Product.this, "Add First Product General Data.", Toast.LENGTH_SHORT).show();

            }
        });
        link.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(!product_id.isEmpty())
                {
                    txtfinish.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.add_product_container, new Link_Form()).commit();
                }

                else
                    Toast.makeText(Add_Product.this, "Add First Product General Data.", Toast.LENGTH_SHORT).show();

            }
        });

        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!product_id.isEmpty())
                {
                    txtfinish.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.add_product_container, new Data_Form()).commit();

                }
                else
                    Toast.makeText(Add_Product.this, "Add First Product General Data.", Toast.LENGTH_SHORT).show();


            }
        });

        discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!product_id.isEmpty())
                {
                    txtfinish.setVisibility(View.VISIBLE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.add_product_container, new Discount_Form()).commit();


                }
                else
                    Toast.makeText(Add_Product.this, "Add First Product General Data.", Toast.LENGTH_SHORT).show();

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActContext(),Manage_Product.class);
                startActivity(intent);
                finish();
                if (product_id.isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Your product is added sucessfully.",Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(),"Your product is updated sucessfully.",Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public Context getActContext()
    {
        return this;
    }
}
