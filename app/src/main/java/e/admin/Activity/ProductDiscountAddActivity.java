package e.admin.Activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.helper.CreateRoundedView;
import e.admin.helper.StartActProcess;
import e.admin.view.GenerateAlertBox;

public class ProductDiscountAddActivity  extends AppCompatActivity {
    GeneralFunctions generalFunc;

    TextView titleTxt;
    ImageView backImgView;
    EditText quantityBox;
    EditText priorityBox;
    EditText priceBox;
    TextView dateStartSelectTxtView;
    TextView dateEndSelectTxtView;
    View viewline;
    private int mYear, mMonth, mDay, mHour, mMinute;
    RadioButton rd_special,rd_discount;
    ArrayList<String> customerGroupDataID = new ArrayList<>();
    ArrayList<String> customerGroupDataList = new ArrayList<>();

    Spinner customerGroupSpinner;

    Button productInfoAddBtn;
    String p_price="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_discount_add);

        viewline=findViewById(R.id.viewline);
        rd_special=findViewById(R.id.rd_special);
        rd_discount=findViewById(R.id.rd_discount);
        quantityBox=findViewById(R.id.edtquantity);
        generalFunc = new GeneralFunctions(getActContext());
        titleTxt = findViewById(R.id.titleTxt);
        dateStartSelectTxtView = findViewById(R.id.txtfromdate);
        dateEndSelectTxtView = findViewById(R.id.txtenddate);
        backImgView = findViewById(R.id.backImgView);
        customerGroupSpinner = findViewById(R.id.customerGroupSpinner);

        //  quantityBox = findViewById(R.id.quantityBox);
        priorityBox = findViewById(R.id.edtpri);
        priceBox = findViewById(R.id.edtprice);
        productInfoAddBtn =  findViewById(R.id.discount);

        productInfoAddBtn.setOnClickListener(new setOnClickList());
        productInfoAddBtn.setId(Utils.generateViewId());
        backImgView.setOnClickListener(new setOnClickList());
        dateStartSelectTxtView.setOnClickListener(new setOnClickList());
        dateEndSelectTxtView.setOnClickListener(new setOnClickList());
        p_price=getIntent().getStringExtra("p_price");
        Log.e("p",""+p_price);
        setLabels();


        new CreateRoundedView(Color.parseColor("#FFFFFF"), Utils.dipToPixels(getActContext(), 5), Utils.dipToPixels(getActContext(), 1), Color.parseColor("#DEDEDE"), dateStartSelectTxtView);

        new CreateRoundedView(Color.parseColor("#FFFFFF"), Utils.dipToPixels(getActContext(), 5), Utils.dipToPixels(getActContext(), 1), Color.parseColor("#DEDEDE"), dateEndSelectTxtView);

        new CreateRoundedView(Color.parseColor("#FFFFFF"), Utils.dipToPixels(getActContext(), 5), Utils.dipToPixels(getActContext(), 1), Color.parseColor("#DEDEDE"), customerGroupSpinner);

        //  dateStartSelectTxtView.setText(getCurrentDate());
        // dateEndSelectTxtView.setText(getCurrentDate());
        rd_special.setOnClickListener(new setOnClickList());
        rd_discount.setOnClickListener(new setOnClickList());
        continueExecution();
    }

    public void setLabels() {
//        titleTxt.setText(getIntent().getStringExtra("PAGE_MODE").equals("DISCOUNT") ? "Add Discount" : "Add Special");
        titleTxt.setText("Add Discount");
       /* quantityBox.setBothText("Quantity", "Enter quantity");
        priorityBox.setBothText("Priority", "Enter priority");
        priceBox.setBothText("Price", "Enter price");
*/
  /*      if (!getIntent().getStringExtra("PAGE_MODE").equals("DISCOUNT")) {
            quantityBox.setVisibility(View.GONE);

            productInfoAddBtn.setText("Add Product Special");
        } else {

            productInfoAddBtn.setText("Add Product Discount");
        }

        quantityBox.setInputType(InputType.TYPE_CLASS_NUMBER);
  */      priorityBox.setInputType(InputType.TYPE_CLASS_NUMBER);
        priceBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
    }

    public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    public void continueExecution() {
        getProductDetails();
    }

    public void getProductDetails() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getStoreProductInfo");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", getIntent().getStringExtra("product_id"));
        parameters.put("isLoadGeneralData", "Yes");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        displayInformation(responseString);
                    } else {
                        generatePageError();
//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generatePageError();
                }
            }
        });
        exeWebServer.execute();

    }

    public void displayInformation(String responseString) {
        customerGroupDataID.clear();
        customerGroupDataList.clear();

        JSONObject productData = generalFunc.getJsonObject("ProductData", responseString);
        JSONArray productRewardData = generalFunc.getJsonArray("ProductRewardData", responseString);
        String productTag = generalFunc.getJsonValue("ProductTag", responseString);
        JSONObject productDescriptionData = generalFunc.getJsonObject("ProductDescriptionData", responseString);

        JSONArray customerGroupDataArr = generalFunc.getJsonArray("CustomerGroupData", generalFunc.getJsonObject("GeneralData", responseString));
        if (productData == null || productDescriptionData == null) {
            generatePageError();
            return;
        }

        if (customerGroupDataArr != null) {
            for (int i = 0; i < customerGroupDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(customerGroupDataArr, i);
                customerGroupDataID.add(generalFunc.getJsonValue("customer_group_id", obj_temp));
                customerGroupDataList.add(generalFunc.getJsonValue("name", obj_temp));
            }
        }

        ArrayAdapter<String> customerGroupAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, customerGroupDataList);
        customerGroupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        customerGroupSpinner.setAdapter(customerGroupAdapter);

    }

    public void generatePageError() {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                generateAlert.closeAlertBox();

                if (btn_id == 1) {
                    backImgView.performClick();
                }
            }
        });
        generateAlert.setContentMessage("", "Some error occurred. Please check your internet or try again later.");
        generateAlert.setPositiveBtn("OK");

        generateAlert.showAlertBox();
    }

    public void chooseStartDate() {
        selectDate(dateStartSelectTxtView, "");

    }

    public void chooseEndDate() {
        if(!dateStartSelectTxtView.getText().toString().equals("From date"))
            selectDate(dateEndSelectTxtView,"end");
        else
            Toast.makeText(this, "Select from date.", Toast.LENGTH_SHORT).show();
    }
    public void selectDate(final TextView textView, String end)
    {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        textView.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        if(end.equals("end"))
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = sdf.parse(dateStartSelectTxtView.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long millis = date.getTime();
            datePickerDialog.getDatePicker().setMinDate(millis);
        }
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void updateData() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", getIntent().getStringExtra("product_id"));

        if (rd_discount.isChecked()) {
            parameters.put("type", "addProductDiscount");
            parameters.put("quantity", quantityBox.getText().toString().trim());

            //   parameters.put("quantity", Utils.getText(quantityBox));
        } else {
            parameters.put("type", "addProductSpecial");
        }

        parameters.put("priority", Utils.getText(priorityBox));
        parameters.put("price", Utils.getText(priceBox));
        parameters.put("date_start", dateStartSelectTxtView.getText().toString());
        parameters.put("date_end", dateEndSelectTxtView.getText().toString());
        parameters.put("customer_group_id", customerGroupDataID.get(customerGroupSpinner.getSelectedItemPosition()));
        Log.d("dataData",parameters.toString().replace(",","&"));
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        (new StartActProcess(getActContext())).setOkResult();
                        backImgView.performClick();
                    } else {
                        generatePageError();
//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generatePageError();
                }
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {
        return ProductDiscountAddActivity.this;
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.backImgView) {
                ProductDiscountAddActivity.super.onBackPressed();

            } else if (i == R.id.txtfromdate) {
                dateEndSelectTxtView.setText("To date");
                chooseStartDate();

            } else if (i == R.id.txtenddate) {
                chooseEndDate();

            } else if (i == productInfoAddBtn.getId()) {
                checkData();

            }
            else if(i==rd_special.getId())
            {
                checkRadio();
            }
            else if(i==rd_discount.getId())
                checkRadio();
        }
    }

    private void checkRadio() {
        if(rd_discount.isChecked()) {
            quantityBox.setVisibility(View.VISIBLE);
        }
        else {
            quantityBox.setVisibility(View.GONE);
        }

    }

    private void checkData() {

        if(!Utils.checkText(priceBox)){
            priceBox.setError("Required");
            return;
        }
        int p,p_pri;
        p_pri= Integer.parseInt(p_price);
        p= Integer.parseInt(priceBox.getText().toString().trim());
        if (p >= p_pri)
        {
            Toast.makeText(this, "Discount should be less than actual product price.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(dateStartSelectTxtView.getText().toString().equals("From date")||dateEndSelectTxtView.getText().toString().equals("To date"))
        {
            Toast.makeText(this, "Select discount date.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(dateStartSelectTxtView.getText().toString().trim().equals(dateEndSelectTxtView.getText().toString().trim()))
        {
            Toast.makeText(this, "Same date not allowed.", Toast.LENGTH_SHORT).show();
            return;

        }


        updateData();
    }
}


