package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import e.admin.Adapter.AdapterChangeStatus;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.view.GenerateAlertBox;

public class ChangeOrderStatus extends AppCompatActivity  {

    TextView tool_txt,done;
    ImageView back;
  //  AdapterChangeStatus adapter;
    RecyclerView status_list;
    ArrayList<HashMap<String, String>> dataList1 = new ArrayList<>();
    List<String> status_id_list=new ArrayList<>();
    GeneralFunctions generalFunc;
    Spinner spinner_status;
    String Order_status_id="";
    ArrayList<String> status_array =new ArrayList<>();
    ArrayAdapter aa;
    HashMap<String, String> OrderData = null;
    TextView order_id,p_name,u_name,email,telephone,city,paymenttype,price,status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_order_status);

        initialize();
    }
    public void initialize()
    {

        generalFunc=new GeneralFunctions(ChangeOrderStatus.this);
        done=findViewById(R.id.done);
        order_id=findViewById(R.id.order_id);
        p_name=findViewById(R.id.p_name);
        u_name=findViewById(R.id.u_name);
        email=findViewById(R.id.email);
        telephone=findViewById(R.id.telephone);
        city=findViewById(R.id.city);
        paymenttype=findViewById(R.id.shippingtype);
        price=findViewById(R.id.price);
        status=findViewById(R.id.status);




        spinner_status=findViewById(R.id.spinner_status);
        status_list=findViewById(R.id.status_list);
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        tool_txt.setText("Change Order Status");
        OrderData = (HashMap<String, String>) getIntent().getSerializableExtra("oderDATA");


        setData();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Order_status_id.isEmpty()) {
                    updatestatus();
                }
                //startActivity(new Intent(getApplicationContext(), CustomOrder.class));
            }
        });


        getList();


    }

    /*private  void  updatestatus()
    {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "changeStatusOfOrder");
        parameters.put("order_id", OrderData.get("order_id"));
        parameters.put("order_status_id", Order_status_id);
        parameters.put("product_id", OrderData.get("product_id"));
        parameters.put("productName", OrderData.get("productName"));
        parameters.put("seller_id", generalFunc.getMemberId());
     //   parameters.put("comment", edtComment.getText().toString());
        parameters.put("isSeller", "1");

        Log.e("data",parameters.toString());
        //  if(check_notify_customer.isChecked())
        parameters.put("customer_notify", "1");
       *//* else
            parameters.put("customer_notify", "0");
*//*

        Log.d("data",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ChangeOrderStatus.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        GenerateAlertBox alertBox= generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if(btn_id==1)
                                {
                                    Intent intent=new Intent(ChangeOrderStatus.this,CustomOrder.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

*/

    private void updatestatus() {



        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "updateOrderStatus");
        parameters.put("order_status_id",Order_status_id);
        parameters.put("order_id",OrderData.get("order_id"));
        parameters.put("product_id",OrderData.get("product_id"));

        Log.d("parater",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ChangeOrderStatus.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString)
            {
                if (responseString != null && !responseString.equals("")) {


                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        Toast.makeText(ChangeOrderStatus.this, generalFunc.getJsonValue("message",responseString), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ChangeOrderStatus.this,CustomOrder.class);
                        startActivity(intent);
                        finish();
                        // Toast.makeText(EditArticleList.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ChangeOrderStatus.this, "" + generalFunc.getJsonValue("message", responseString), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        exeWebServer.execute();



    }

    private void setData() {

        if (OrderData != null) {
            order_id.setText(OrderData.get("order_id"));
            p_name.setText(OrderData.get("product_name"));
            u_name.setText(OrderData.get("firstname")+" "+OrderData.get("lastname"));
            email.setText(OrderData.get("email"));
           telephone.setText(OrderData.get("telephone"));
           city.setText(OrderData.get("shipping_city"));
          paymenttype.setText(OrderData.get("payment_method"));
           price.setText(OrderData.get("total"));
           status.setText(OrderData.get("status_name"));



        }

    }
    private void getList() {

        dataList1.clear();
        status_array.clear();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getOrderStatus");
//parameters.put("customer_id",generalFunc.getMemberId());


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(ChangeOrderStatus.this, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @SuppressLint("ResourceType")
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);

                        if (msgArr != null) {
                            status_array.add("Chanage Order Status");
                            for (int i = 0; i < msgArr.length(); i++)
                            {
                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("order_status_id", generalFunc.getJsonValue("order_status_id", obj_service));
                                dataMap_products.put("language_id", generalFunc.getJsonValue("language_id", obj_service));
                                dataMap_products.put("name", generalFunc.getJsonValue("name", obj_service));

                                status_array.add(generalFunc.getJsonValue("name",obj_service));
                                dataList1.add(dataMap_products);
                            }
                            aa = new ArrayAdapter(ChangeOrderStatus.this,android.R.layout.simple_spinner_item,status_array);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinner_status.setAdapter(aa);
                            spinner_status.setSelection(getPosition(OrderData.get("order_status_id")));
                            spinner_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                                {


                                    if(i>0)
                                    {
                                        Order_status_id=dataList1.get(i-1).get("order_status_id");


                                    }
                                    else
                                    {
                                        Order_status_id ="";


                                    }
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                }
                            });
                        }
                    } else {

                    }
                } else {

                }
            }
        });
        exeWebServer.execute();

    }


    private int getPosition(String category_id)
    {
        for(int i=0;i<dataList1.size();i++)
        {
            if(dataList1.get(i).get("order_status_id").equals(category_id))
                return i+1;
        }
        return 0;
    }





}
