package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.Login;

import java.util.HashMap;

import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.utils.SharedPref;

public class LogIn extends AppCompatActivity {

    Button login;
    EditText email,password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public GeneralFunctions generalFunc;
    TextView  show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        initialize();
    }
    public void initialize()
    {
        generalFunc = new GeneralFunctions(getActContext());
        password=findViewById(R.id.password);
        show = findViewById(R.id.show);

        email=findViewById(R.id.email);
        login=findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkvalidation())
                    userlogin();
                //startActivity(new Intent(getApplicationContext(),Dashboard.class));
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPassword(password, show);
            }
        });
    }

    private boolean checkvalidation()
    {
        if (email.getText().toString().isEmpty()) {
            email.setError("Emailid is required");
            return false;
        } else if (!email.getText().toString().matches(emailPattern)) {
            email.setError("Emailid should be in correct format");
            return false;
        }

        if (password.getText().toString().isEmpty()) {
            password.setError("Password is required");
            return false;
        }

        return true;
    }

    private void showPassword(EditText password, TextView show) {

        if (show.getText().toString().equals("Show")) {
            show.setText("Hide");
            password.setInputType(InputType.TYPE_CLASS_TEXT);
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");
            password.setTypeface(type);
        } else {
            show.setText("Show");
            Typeface type = Typeface.createFromAsset(getAssets(),"font/montserratregular.ttf");

            password.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            password.setTypeface(type);
        }
    }

    private void userlogin() {

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "signInadmin");
        parameters.put("email",email.getText().toString() );
        parameters.put("password", password.getText().toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail) {
                        generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                        generalFunc.storedata("last_message_id",generalFunc.getJsonValue("last_message_id",responseString));
                        generalFunc.storeUserData(generalFunc.getJsonValue(Utils.message_str, responseString));
                        String country_id = generalFunc.getJsonValue("country_id", responseString);
                        SharedPref sharedPref=new SharedPref();
                        SharedPref.WriteSharePrefrence(getActContext(),"userCode",generalFunc.getJsonValue("userCode",responseString));
                        Toast.makeText(LogIn.this, "You are login successfully", Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(getActContext(), Dashboard.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActContext().startActivity(intent);
                        finish();

/* GenerateAlertBox alertBox= generalFunc.showGeneralMessage("Sucessfully","You are login sucessfully in EASYSHOP" );
alertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
@Override
public void handleBtnClick(int btn_id) {
if(btn_id==1)
{
Intent intent=new Intent(getActContext(), Dashboard.class);
intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
getActContext().startActivity(intent);
finish();
//finishAffinity();
}
}
});*/
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }




    private Context getActContext() {
        return LogIn.this;
    }
}
