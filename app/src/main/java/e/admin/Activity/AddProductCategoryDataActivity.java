package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import e.admin.Adapter.AdapterRecycler;
import e.admin.Model.ProductCategoriesData;
import e.admin.Model.ResponseSpinner;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.helper.API;
import e.admin.view.GenerateAlertBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddProductCategoryDataActivity extends AppCompatActivity {

    TextView titleTxt;
    ImageView backImgView;
    private static final String TAG = "AddProductCategoryDataA";
    GeneralFunctions generalFunc;
    ProgressBar loading;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    Spinner sp_color;
    Spinner sp_size;
    List<Object> list;
    List<String> optionData=new ArrayList<>();
    ResponseSpinner.Message message;
    EditText productPriceBox;
    ResponseSpinner.Message.Data[]  data;
    // MaterialEditText sizeBox;
    //  MaterialEditText colorBox;
    Button productCategoryAddBtn;
    RecyclerView categoriesList;
    View categoriesListView;
    TextView noDataArea;

    Spinner priceSpinner;
    private ArrayList<String> priceDataList = new ArrayList<>();
    private ArrayList<String> currencyCountryIdList = new ArrayList<>();
    ArrayAdapter<String> priceAdapter;
    public static ArrayList<String> currencySymbolList = new ArrayList<>();
    String currencySymbol = "$";

    String arr[];
    String OPTION_ID="",OPTION_VALU_ID="",OPTION_VALUE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_category_data);
        generalFunc = new GeneralFunctions(getActContext());
        sp_color=findViewById(R.id.color_spinner);
        sp_size=findViewById(R.id.size_spinner);
        titleTxt = findViewById(R.id.titleTxt);
        backImgView = findViewById(R.id.backImgView);
        loading = findViewById(R.id.loading);


        productCategoryAddBtn = ( findViewById(R.id.productCategoryAddBtn));
        productCategoryAddBtn.setId(Utils.generateViewId());
        productPriceBox = findViewById(R.id.productPriceBox);
        //sizeBox = findViewById(R.id.sizeBox);
        //colorBox = findViewById(R.id.colorBox);
        priceSpinner = findViewById(R.id.priceSpinner);
        noDataArea = findViewById(R.id.noDataArea);
        categoriesList = findViewById(R.id.categoriesList);
        categoriesListView = findViewById(R.id.categoriesListView);

        productCategoryAddBtn.setOnClickListener(new setOnClickList());
        backImgView.setOnClickListener(new setOnClickList());

        setLabel();
        getOptionList();
        getCategoryData();

        //color array  set



        priceAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, Utils.currencySymbolList);
        priceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        priceSpinner.setAdapter(priceAdapter);



    }

    private void getOptionList()
    {
        optionData.clear();
        dataList.clear();
        optionData.add("select type");
        HashMap<String, String> parameters = new HashMap<String, String>();
        // parameters.put("type", "getProductCategoryData");
        parameters.put("type", "getOptionType");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    if (generalFunc.getJsonValue(Utils.action_str, responseString).equals("1")) {

                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                HashMap<String,String> map=new HashMap<>();
                                map.put("option_id",generalFunc.getJsonValue("option_id",obj_temp));
                                map.put("type",generalFunc.getJsonValue("type",obj_temp));
                                map.put("sort_order",generalFunc.getJsonValue("sort_order",obj_temp));

                                optionData.add(generalFunc.getJsonValue("type",obj_temp));
                                dataList.add(map);
                            }

                            ArrayAdapter<String> adpater_color;
                            adpater_color = new ArrayAdapter<String>(getActContext(), android.R.layout.simple_list_item_1, optionData);
                            sp_color.setAdapter(adpater_color);

                            sp_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    if(position>0)
                                        getDataSpinner(dataList.get(position-1).get("option_id"));

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            //setAdapter();


                        } else {
                            //setNoDataArea(true);
                        }
                    } else {
                        //setNoDataArea(true);
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void getDataSpinner(String option_id)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final API request = retrofit.create(API.class);
        Call<ResponseSpinner> call = request.getSpinnerData("getAvailableOptions",option_id);
        call.enqueue(new Callback<ResponseSpinner>() {
            @Override
            public void onResponse(Call<ResponseSpinner> call, Response<ResponseSpinner> response)
            {
                if(response.body().getAction().equals("1"))
                {
                    message=response.body().getMessage();
                    data=message.getOptionValueDescData();
                    if(data.length>0)
                    {
                        arr=new String[data.length];
                        for(int i=0;i<arr.length;i++)
                        {
                            arr[i]=data[i].getName();
                        }
                        //size array set
                        ArrayAdapter<String> adpater_size = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, arr);
                        sp_size.setAdapter(adpater_size);

                        sp_size.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                OPTION_ID = data[position].getOption_id();
                                OPTION_VALU_ID=data[position].getOption_value_id();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseSpinner> call, Throwable t) {

            }
        });
    }
    private void getCategoryData() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        // parameters.put("type", "getProductCategoryData");
        parameters.put("type", "getProductOptions");
        parameters.put("product_id",getIntent().getStringExtra("product_id"));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    if (generalFunc.getJsonValue(Utils.action_str, responseString).equals("1")) {
                        list = new ArrayList<>();
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        if (msgArr != null) {
                            for (int i = 0; i < msgArr.length(); i++) {
                                JSONObject obj_temp = generalFunc.getJsonObject(msgArr, i);
                                ProductCategoriesData data = new ProductCategoriesData();

                          /*      data.setId(generalFunc.getJsonValue(ProductCategoriesData.s.id, obj_temp));
                                data.setProduct_id(generalFunc.getJsonValue(ProductCategoriesData.s.product_id, obj_temp));
                                data.setSize(generalFunc.getJsonValue(ProductCategoriesData.s.size, obj_temp));
                                data.setColor(generalFunc.getJsonValue(ProductCategoriesData.s.color, obj_temp));*/
                                data.setOption_value_name(generalFunc.getJsonValue("option_value_name", obj_temp));
                                data.setPrice_prefix(generalFunc.getJsonValue("price_prefix", obj_temp));
                                data.setOption_name(generalFunc.getJsonValue("option_name", obj_temp));
                                data.setPrice(generalFunc.getJsonValue(ProductCategoriesData.s.price, obj_temp));
                                data.setProduct_option_id(generalFunc.getJsonValue("product_option_id", obj_temp));
                                data.setProduct_option_value_id(generalFunc.getJsonValue("product_option_value_id",obj_temp));
                                data.setProduct_id(generalFunc.getJsonValue("product_id",obj_temp));

                                list.add(data);
                            }

                            setNoDataArea(false);
                            setAdapter();

                        } else {
                            setNoDataArea(true);
                        }
                    } else {
                        setNoDataArea(true);
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    private void setAdapter() {
        AdapterRecycler adapter = new AdapterRecycler(this, list, R.layout.design_product_category_data_list);
        categoriesList.setAdapter(adapter);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        categoriesList.setLayoutManager(mLayoutManager);

        adapter.setOnBind(new AdapterRecycler.OnBind() {
            @Override
            public void onBindViewHolder(View view, int position) {

                TextView size = view.findViewById(R.id.size);
                TextView color = view.findViewById(R.id.color);
                TextView price = view.findViewById(R.id.price);
                View container = view.findViewById(R.id.container);

                final ProductCategoriesData data = (ProductCategoriesData) list.get(position);

                size.setText(data.getOption_name());
                color.setText(data.getOption_value_name());
                price.setText(data.getPrice_prefix()+""+(data.getPrice()));

                container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
                        generateAlert.setCancelable(false);
                        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                            @Override
                            public void handleBtnClick(int btn_id) {
                                if (btn_id == 1) {
                                    generateAlert.closeAlertBox();
                                    removeCategoryData(data.getProduct_option_id(), data.getProduct_option_value_id(),data.getProduct_id());

                                } else if (btn_id == 0) {
                                    generateAlert.closeAlertBox();
                                }
                            }
                        });
                        generateAlert.setContentMessage("Confirm", "Are you sure, you want to remove selected Category?");
                        generateAlert.setPositiveBtn("YES");
                        generateAlert.setNegativeBtn("NO");
                        generateAlert.showAlertBox();
                    }
                });
            }
        });
    }

    private void removeCategoryData(String product_option_id, String product_option_value_id, String productId) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "removeProductCategoryData");
        parameters.put("product_id", productId);
        parameters.put("product_option_id", product_option_id);
        parameters.put("product_option_value_id", product_option_value_id);


        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {
                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                    getCategoryData();
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    void setNoDataArea(boolean isNoDataArea) {
        if (isNoDataArea) {
            noDataArea.setVisibility(View.VISIBLE);
            categoriesListView.setVisibility(View.GONE);
        } else {
            noDataArea.setVisibility(View.GONE);
            categoriesListView.setVisibility(View.VISIBLE);
        }
    }

    private String getCurrencySymbol() {
        return (String) priceSpinner.getSelectedItem();
    }
/*
    private String getPrice(String price) {
        return Utils.getPriceValueDollar(price, (String) priceSpinner.getSelectedItem());
    }*/

    private void setLabel() {
        productPriceBox.setHint( "Enter Product Price");
        //sizeBox.setBothText("Product Size", "Enter Product Size");
        // colorBox.setBothText("Product Color", "Enter Product Color");

        productCategoryAddBtn.setText("Add Category");
        titleTxt.setText("Add Categories");

    }

    /*  private void setCurrencyList() {
          HashMap<String, String> param = new HashMap<>();
          param.put("type", "getAllCurrency");
          ExecuteWebServerUrl url = new ExecuteWebServerUrl(param);
          url.execute();
          url.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
              @Override
              public void setResponse(String responseString) {
                  if (generalFunc.checkResponseString(responseString, false) == 1) {
                      ArrayList<HashMap<String, String>> list = generalFunc.getJsonMapList(responseString);
                      setCurrencyDialogList(list);
                  }
              }
          });
      }
  */
  /*  private void setCurrencyDialogList(ArrayList<HashMap<String, String>> list) {
        priceDataList.clear();
        currencyCountryIdList.clear();
        Utils.currencyList.clear();

        for (HashMap<String, String> map : list) {

            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(map);
            CurrencyTable pojo = gson.fromJson(jsonElement, CurrencyTable.class);

            StringBuilder builder = new StringBuilder();

           *//* if (generalFunc.isValid(map.get("symbol_right"))){
                builder.append(map.get("symbol_right"));
                currencyCountryIdList.add(map.get("symbol_right"));

            }else {
                builder.append(map.get("symbol_left"));
                currencyCountryIdList.add(map.get("symbol_left"));

            }*//*
            if (generalFunc.isValid(map.get("symbol_left"))) {
                builder.append(map.get("symbol_left"));
                currencyCountryIdList.add(map.get("symbol_left"));
                Log.e(TAG, "setCurrencyDialogList: " +map.get("symbol_left"));
                //      Toast.makeText(this, ""+map.get("symbol_left"), Toast.LENGTH_SHORT).show();
            } else {
                builder.append(map.get("symbol_right"));
                currencyCountryIdList.add(map.get("symbol_right"));
                //Toast.makeText(this, ""+pojo.getSymbolRight(), Toast.LENGTH_SHORT).show();

            }
            Utils.currencyList.add(map.get("value"));
            map.clear();
            priceDataList.add(builder.toString());
        }

        priceAdapter.notifyDataSetChanged();
        currencySymbolList = currencyCountryIdList;

        for (int i = 0; i < priceDataList.size(); i++) {
            try {

                if (Utils.currencySymbolList != null && currencySymbol.equals(Utils.currencySymbolList.get(i).trim()))
                    priceSpinner.setSelection(i);
            }catch (Exception e){}
        }
    }
*/
    private void saveCategory() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        //   parameters.put("type", "saveAddProductCategory");
        parameters.put("type", "updateProductOptions");
        parameters.put("product_id", getIntent().getStringExtra("product_id"));
        //parameters.put("size", sizeName);//Utils.getText(sizeBox));
        // parameters.put("color", colorName);//Utils.getText(colorBox));
        parameters.put("option_value_id", OPTION_VALU_ID);//Utils.getText(colorBox));
        parameters.put("option_id", OPTION_ID);//Utils.getText(colorBox));
        //    parameters.put("option_value_name", OPTION_ID);//Utils.getText(colorBox));
        parameters.put("price", Utils.getText(productPriceBox));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {
                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                    getCategoryData();
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {
        return AddProductCategoryDataActivity.this;
    }

    private class setOnClickList implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (v.getId() == backImgView.getId()) {
                AddProductCategoryDataActivity.super.onBackPressed();
            } else if (v.getId() == productCategoryAddBtn.getId()) {
                if(!OPTION_ID.isEmpty()&&!OPTION_VALU_ID.isEmpty())
                    saveCategory();
                else
                    Toast.makeText(AddProductCategoryDataActivity.this, "Select product option.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}


