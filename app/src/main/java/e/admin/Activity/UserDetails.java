package e.admin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.CollectionPointListAdapter;
import e.admin.Adapter.UserDetailAdapter;
import e.admin.R;

public class UserDetails extends AppCompatActivity {

    TextView tool_txt;
    ImageView back;
    ImageView add;
    RecyclerView user_data_list;
    UserDetailAdapter adapter;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        initialize();
    }
    public void initialize()
    {
        tool_txt=findViewById(R.id.tool_txt);
        back=findViewById(R.id.back);
        add=findViewById(R.id.add);
        user_data_list=findViewById(R.id.user_data_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tool_txt.setText("User Detail");

        add.setVisibility(View.GONE);
        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(UserDetails.this, LinearLayoutManager.VERTICAL, false);
        user_data_list.setLayoutManager(layoutManager);
        adapter = new UserDetailAdapter(this);
        user_data_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        //getMessages();


    }
}
