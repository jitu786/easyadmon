package e.admin.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import e.admin.Activity.Add_Product;
import e.admin.Adapter.ProductImagesRecyclerAdapter;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.helper.ImageFilePath;
import e.admin.helper.UploadImage;
import e.admin.view.GenerateAlertBox;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class Image_Form extends Fragment implements View.OnClickListener, ProductImagesRecyclerAdapter.OnItemClickListener, UploadImage.SetResponseListener
{

    private static final int IMAGE_PICKER_SELECT = 987;
    private static final int CAMERA_REQUEST = 999;
    LinearLayout lyt_galalry,camera;
    ImageView img;
    Add_Product manageProductAct;
    GeneralFunctions generalFunc;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static String defaultImageData;
    public static String productId;
    Button productInfoAddBtn;
    TextView noImgTxtView;
    Uri fileUri=null;
    TextView defaultText;
    TextView addMoreImgTxtView;
    public static final String IMAGE_DIRECTORY_NAME = "Temp";

    RecyclerView productImagesRecyclerView;
    ProductImagesRecyclerAdapter adapter;

    ArrayList<HashMap<String, String>> listData = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.image_form, container, false);
        initlized(v);
        return v;
    }

    private void initlized(View v)
    {
        generalFunc=new GeneralFunctions(getActContext());
        manageProductAct=(Add_Product) getActivity();
        noImgTxtView=v.findViewById(R.id.noImgTxtView);
        defaultText=v.findViewById(R.id.defaultText);
        lyt_galalry=v.findViewById(R.id.lyt_galalry);
        productImagesRecyclerView=v.findViewById(R.id.productImagesRecyclerView);
        camera=v.findViewById(R.id.camera);
        img=v.findViewById(R.id.img);
        lyt_galalry.setOnClickListener(this);
        camera.setOnClickListener(this);
        productImagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActContext()));
        adapter = new ProductImagesRecyclerAdapter(getActContext(), listData, generalFunc, false);
        adapter.setOnItemClickListener(this);
        productImagesRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getProductImages();
    }

    private Context getActContext() {
        return getContext();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.lyt_galalry:
                selectImageGalary();
                break;
            case R.id.camera:
                selectCamera();
                break;
        }
    }

    private void selectImageGalary()
    {
        if(ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
        }
        else {
            Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);startActivityForResult(cameraIntent, IMAGE_PICKER_SELECT);
        }
    }
    private void selectCamera()
    {
        if (ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                ||ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED||
                ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1223);
        }
        else
        {
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, CAMERA_REQUEST);

        }
    }
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Error", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICKER_SELECT) {
                Uri returnUri = data.getData();
                CropImage.activity(returnUri)
                        .setBackgroundColor(R.color.white_50)
                        .setBorderLineColor(Color.RED)
                        .setOutputCompressQuality(100)
                        .start(getActContext(), this);
            } else if (requestCode == CAMERA_REQUEST)
            {
                CropImage.activity(fileUri)
                        .setBackgroundColor(R.color.white_50)
                        .setBorderLineColor(Color.RED)
                        .setOutputCompressQuality(50)
                        .start(getActContext(),this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == Activity.RESULT_OK) {
                    uploadImage(null,result.getUri());
                }
            }
        }
    }

    @Override
    public void onItemClickList(View v, int btn_type, int position) {
        if(btn_type==1)
        {
            confirmDeleteProductImg(position);
        }
    }
    public void getProductImages() {
        noImgTxtView.setVisibility(View.GONE);
        //  containerView.setVisibility(View.GONE);

        listData.clear();
        adapter.notifyDataSetChanged();

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("type", "getSellerProductImages");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        productId = manageProductAct.product_id;
                        displayInformation(responseString);
                    } else {
//                        generatePageError();
                    }

                } else {
                    //                  generatePageError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void displayInformation(String responseString) {

        JSONArray messageData = generalFunc.getJsonArray(Utils.message_str, responseString);
        defaultImageData = generalFunc.getJsonValue(Utils.defaultImage, responseString);

        if (messageData == null) {
            //generatePageError();
            return;
        }

        if (messageData.length() > 0) {

            for (int i = 0; i < messageData.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(messageData, i);

                HashMap<String, String> mapData = new HashMap<>();
                mapData.put("imageURL", generalFunc.getJsonValue("imageURL", obj_temp));
                mapData.put("product_image_id", generalFunc.getJsonValue("product_image_id", obj_temp));
                //  mapData.put("TYPE", "" + ProductImagesRecyclerAdapter.TYPE_ITEM);

                listData.add(mapData);
            }
            adapter.notifyDataSetChanged();
            noImgTxtView.setVisibility(View.GONE);
        } else {
            noImgTxtView.setVisibility(View.VISIBLE);
        }
        //containerView.setVisibility(View.VISIBLE);
        if (noImgTxtView.getVisibility()== View.VISIBLE) defaultText.setVisibility(View.GONE);
        else defaultText.setVisibility(View.VISIBLE);
    }
    public void deleteProductImage(int position) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "deleteSellerProductImages");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);
        parameters.put("product_image_id", listData.get(position).get("product_image_id"));

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        getProductImages();

                    }

                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void uploadImage(Bitmap bitmapImage, Uri returnUri)
    {
        Log.d("fileUri",returnUri.toString());
        ArrayList<String[]> paramsList = new ArrayList<>();
        paramsList.add(Utils.generateImageParams("customer_id", generalFunc.getMemberId()));

        paramsList.add(Utils.generateImageParams("product_id", manageProductAct.product_id));
        paramsList.add(Utils.generateImageParams("type", "addSellerProductImages"));
        String selectedImagePath="";
        selectedImagePath= new ImageFilePath().getPath(getActContext(), returnUri);
        Log.d("path",selectedImagePath);

        if (Utils.isValidImageResolution(selectedImagePath) )
        {
            Log.d("path",selectedImagePath);
//                    new UploadImage(getActContext(), selPath, Utils.TempProfileImageName, paramsList,imgSelectType).execute();

            UploadImage uploadImg = new UploadImage(getActContext(), selectedImagePath, Utils.TempProfileImageName, paramsList, "");
            uploadImg.setLoadingMessage("Uploading your product's image...");
            uploadImg.setResponseListener(this);
            uploadImg.execute();
        } else {
            generalFunc.showGeneralMessage("", Utils.errorImageQuality);
        }

    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    public void onFileUploadResponse(String responseString, String type) {
        boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

        if (isDataAvail) {
            getProductImages();
        } else {
            generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
        }
    }
    public boolean checkImageSize(Uri fileUri)
    {
        int dataSize = 0;
        String scheme = fileUri.getScheme();
        if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
            try {
                InputStream fileInputStream = getApplicationContext().getContentResolver().openInputStream(fileUri);
                dataSize = fileInputStream.available();

            } catch (Exception e) {

                e.printStackTrace();
                return false;
            }
            if (((dataSize / 1024) / 1024) >= 5) {
                fileUri = null;
                Toast.makeText(getActContext(), "This file too large for uploading", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (scheme.equals(ContentResolver.SCHEME_FILE)) {
            File f = null;
            String path = fileUri.getPath();
            try {
                f = new File(path);
            } catch (Exception e) {
                e.printStackTrace();
                return false;

            }
            if (((f.length() / 1024) / 1024) >= 5) {
                fileUri = null;
                Toast.makeText(getActContext(), "This file too large for uploading", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }
    public void confirmDeleteProductImg(final int position) {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                generateAlert.closeAlertBox();

                if (btn_id == 1) {
                    deleteProductImage(position);
                }
            }
        });
        generateAlert.setContentMessage("", "Are you sure to delete image?");
        generateAlert.setPositiveBtn("OK");
        generateAlert.setNegativeBtn("Cancel");

        generateAlert.showAlertBox();
    }
}

