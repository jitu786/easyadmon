package e.admin.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import e.admin.Activity.Add_Product;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.view.GenerateAlertBox;

public class General_Form extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {


    EditText p_name,p_model,p_des,p_price,p_qty,p_min_qty,p_meta_tag,meta_tag_des,meta_tag_kw,p_tag;
    Button next;
    GeneralFunctions generalFunc;
    private boolean isDataAvail=false;
    Add_Product manageProductAct;
    JSONObject productData;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.general_form, container, false);
        initilize(v);
        return v;
    }

    private void initilize(View v)
    {
        manageProductAct = (Add_Product) getActivity();
        generalFunc=new GeneralFunctions(getContext());
        p_name=v.findViewById(R.id.p_name);
        p_model=v.findViewById(R.id.p_model);
        p_des=v.findViewById(R.id.p_des);
        p_price=v.findViewById(R.id.p_price);
        p_qty=v.findViewById(R.id.p_qty);
        p_min_qty=v.findViewById(R.id.p_min_qty);
        p_meta_tag=v.findViewById(R.id.p_meta_tag);
        meta_tag_des=v.findViewById(R.id.meta_tag_des);
        meta_tag_kw=v.findViewById(R.id.meta_tag_kw);
        p_tag=v.findViewById(R.id.p_tag);
        next=v.findViewById(R.id.next);
        next.setOnClickListener(this);
        getProductDetails();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.next:
                checkValidation();
                break;
        }
    }

    private void checkValidation()
    {
        if(p_name.getText().toString().trim().isEmpty())
        {
            p_name.setError("Product name is required.");
            return;
        }else if(p_model.getText().toString().trim().isEmpty())
        {
            p_model.setError("Product model is required.");
            return;
        }else if(p_des.getText().toString().trim().isEmpty())
        {
            p_des.setError("Product description is required.");
            return;
        }else if(p_price.getText().toString().trim().isEmpty())
        {
            p_price.setError("Product price is required.");
            return;
        }else if(p_qty.getText().toString().trim().isEmpty())
        {
            p_qty.setError("Product quantity is required.");
            return;
        }else if(p_min_qty.getText().toString().trim().isEmpty())
        {
            p_min_qty.setError("Product minumum quantity is required.");
            return;
        }else  if (Integer.parseInt(Utils.getText(p_qty)) - 1 < Integer.parseInt(Utils.getText(p_min_qty)))
        {
            Utils.setErrorFields(p_min_qty, "Minimum Quantity should be less than Quantity  ");
            return;
        }
        else if(p_meta_tag.getText().toString().trim().isEmpty())
        {
            p_meta_tag.setError("Product metatag title is required.");
            return;
        }
        else
        {
            updateProduct();
        }

    }

    private void updateProduct()
    {


        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "updateStoreProduct");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id",manageProductAct.product_id);
        parameters.put("product_name", Utils.getText(p_name));
        parameters.put("product_model", Utils.getText(p_model));
        parameters.put("product_description", Utils.getText(p_des));
        // parameters.put("currency_symbol", getCurrencySymbol());
        parameters.put("product_price", Utils.getText(p_price));
        parameters.put("product_quantity", Utils.getText(p_qty));
        parameters.put("product_min_quantity", Utils.getText(p_min_qty));

        parameters.put("meta_tag_title", Utils.getText(p_meta_tag));
        parameters.put("meta_tag_description", Utils.getText(meta_tag_des));
        parameters.put("meta_tag_keywords", Utils.getText(meta_tag_kw));
        parameters.put("product_tag_keywords", Utils.getText(p_tag));
        //      parameters.put("product_tag_keywords", Utils.getText(productTagsBox));
        //    parameters.put("product_type", productTypeList.get(productTypePosition));
//        parameters.put("product_id", manageProductAct.product_id);
        parameters.put("commision", "0");//commission.getText().toString());
        Log.d("data_log",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail) {
                        if (manageProductAct.product_id.equals("")) {
                            String product_id = generalFunc.getJsonValue("product_id", responseString);
                            manageProductAct.product_id = product_id;
                            manageProductAct.setResult(Activity.RESULT_OK);
                            Toast.makeText(getContext(),"Your product is added sucessfully.",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getContext(),"Your product is updated sucessfully.",Toast.LENGTH_SHORT).show();
                        }


                        getProductDetails();
                    }
                    //generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }

    private Context getActContext()
    {
        return getContext();
    }
    public void getProductDetails() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getStoreProductInfo");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.execute();
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        displayInformation(responseString);
                    } else {
                        //generatePageError();
                    }

                } else {
                    generatePageError();
                }
            }
        });

    }

    public void generatePageError() {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                generateAlert.closeAlertBox();

                if (btn_id == 1) {
                    //manageProductAct.backImgView.performClick();
                }
            }
        });
        generateAlert.setContentMessage("", "Some error occurred. Please check your internet or try again later.");
        generateAlert.setPositiveBtn("OK");

        generateAlert.showAlertBox();
    }

    public void displayInformation(String responseString) {

        productData = generalFunc.getJsonObject("ProductData", responseString);
        JSONObject productDescriptionData = generalFunc.getJsonObject("ProductDescriptionData", responseString);
        JSONArray productCategoryDataArr = generalFunc.getJsonArray("ProductCategoryData", responseString);

        if (productData == null || productDescriptionData == null) {
            generatePageError();
            return;
        }

        p_model.setText(generalFunc.getJsonValue("model", productData));

        p_qty.setText(generalFunc.getJsonValue("quantity", productData));
        p_min_qty.setText(generalFunc.getJsonValue("minimum", productData));
        p_price.setText(generalFunc.getJsonValue("price", productData));
      /*  if (!generalFunc.getJsonValue("currency_symbol", productData).equals("")) {
            currencySymbol = generalFunc.getJsonValue("currency_symbol", productData);
            String price = Utils.getPriceValue(generalFunc.getJsonValue("price", productData), currencySymbol);

            //String price =StoreProductsRecyclerAdapter.price;
            //Toast.makeText(manageProductAct, ""+price, Toast.LENGTH_SHORT).show();
            productPriceBox.setText(price);
        }*/
        p_name.setText(generalFunc.getJsonValue("name", productDescriptionData));
        p_des.setText(generalFunc.getJsonValue("description", productDescriptionData));
        p_meta_tag.setText(generalFunc.getJsonValue("meta_title", productDescriptionData));
        meta_tag_des.setText(generalFunc.getJsonValue("meta_description", productDescriptionData));
        meta_tag_kw.setText(generalFunc.getJsonValue("meta_keyword", productDescriptionData));
        p_tag.setText(generalFunc.getJsonValue("tag", productDescriptionData));
        //productTagsBox.setText(generalFunc.getJsonValue("tag", productDescriptionData));

      /*  for (int i = 0; i < priceDataList.size(); i++)
        {

            if (Utils.currencySymbolList != null && currencySymbol.equals(currencySymbolList.get(i).trim()))
                priceSpinner.setSelection(i);
        }*/
        if(productCategoryDataArr.length()==0||productCategoryDataArr==null)
        {
            generalFunc.showGeneralMessage("", "Please add the product details in other section.");
        }
    }

}
