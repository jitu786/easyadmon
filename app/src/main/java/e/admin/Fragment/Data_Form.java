package e.admin.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Activity.AddProductCategoryDataActivity;
import e.admin.Activity.Add_Product;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.helper.StartActProcess;
import e.admin.view.GenerateAlertBox;

public class Data_Form extends Fragment
{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    Add_Product manageProductAct;
    GeneralFunctions generalFunc;
    ArrayList<String> taxClassData = new ArrayList<>();
    ArrayList<String> taxClassDataIds = new ArrayList<>();

    ArrayList<String> lengthClassData = new ArrayList<>();
    ArrayList<String> lengthClassDataIds = new ArrayList<>();

    ArrayList<String> weightClassData = new ArrayList<>();
    ArrayList<String> weightClassDataIds = new ArrayList<>();

    ArrayList<String> subtractStockData = new ArrayList<>();
    ArrayList<String> statusData = new ArrayList<>();

    ArrayList<String> outOfStockStatusData = new ArrayList<>();
    ArrayList<String> outOfStockStatusDataIds = new ArrayList<>();
    Spinner taxClassSpinner;
    Spinner subtractStockSpinner;
    Spinner outOfStockStatusSpinner;
    Spinner lengthClassSpinner;
    Spinner weightClassSpinner;
    Spinner statusSpinner;

    RadioGroup shippingRadioGroup;
    RadioButton shippingYesRadioBtn;
    RadioButton shippingNoRadioBtn;
    EditText lengthBox,widthBox,heightBox,weightBox;
    Button edit_data;
    RelativeLayout categoryAddArea;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.data_form, container, false);

        generalFunc=new GeneralFunctions(getContext());
        manageProductAct=(Add_Product) getActivity();
        outOfStockStatusSpinner=v.findViewById(R.id.spinner_stock_status);
        lengthClassSpinner=v.findViewById(R.id.length);
        weightClassSpinner=v.findViewById(R.id.spinner_wclass);
        statusSpinner=v.findViewById(R.id.spinner_as);
        taxClassSpinner=v.findViewById(R.id.spinner_TaxData);
        subtractStockSpinner=v.findViewById(R.id.spinner_subtrack);
        shippingNoRadioBtn=v.findViewById(R.id.rd_no);
        shippingYesRadioBtn=v.findViewById(R.id.rd_yes);
        lengthBox=v.findViewById(R.id.edtLength);
        widthBox=v.findViewById(R.id.edtwidth);
        heightBox=v.findViewById(R.id.edtHeghitbox);
        weightBox=v.findViewById(R.id.edtweight);
        edit_data=v.findViewById(R.id.edit_data);
        categoryAddArea=v.findViewById(R.id.categoryAddArea);

        getProductDetails();
        edit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProductData();
            }
        });
        categoryAddArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bn = new Bundle();
                bn.putString("product_id", manageProductAct.product_id);

                (new StartActProcess(getActContext())).startActForResult(getCurrentFragment(), AddProductCategoryDataActivity.class, Utils.ADD_CATEGORIES_REQ_CODE, bn);
            }
        });


        return v;
    }
    public Fragment getCurrentFragment() {
        return this;
    }

    public void getProductDetails() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getStoreProductInfo");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        displayInformation(responseString);
                    } else {
                        //generatePageError();
//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generatePageError();
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext() {
        return getContext();
    }

    public void displayInformation(String responseString) {

        JSONObject productData = generalFunc.getJsonObject("ProductData", responseString);
        String ProductSEOURL = generalFunc.getJsonValue("ProductSEOURL", responseString);
        JSONObject productDescriptionData = generalFunc.getJsonObject("ProductDescriptionData", responseString);
        JSONArray taxDataArr = generalFunc.getJsonArray("TaxData", responseString);
        JSONArray weightClassDataArr = generalFunc.getJsonArray("WeightClassData", responseString);
        JSONArray lengthClassDataArr = generalFunc.getJsonArray("LengthClassData", responseString);
        JSONArray stockStatusData = generalFunc.getJsonArray("StockStatusData", responseString);

        if (productData == null || productDescriptionData == null) {
            generatePageError();
            return;
        }


        //     dateSelectTxtView.setText(generalFunc.getJsonValue("date_available", productData).equals("") ? getCurrentDate() : generalFunc.getJsonValue("date_available", productData));
        lengthBox.setText(generalFunc.getJsonValue("length", productData));
        widthBox.setText(generalFunc.getJsonValue("width", productData));
        heightBox.setText(generalFunc.getJsonValue("height", productData));
        weightBox.setText(generalFunc.getJsonValue("weight", productData));
        //  sortOrderBox.setText(generalFunc.getJsonValue("sort_order", productData));
        if (generalFunc.getJsonValue("shipping", productData).equalsIgnoreCase("1")) {
            shippingYesRadioBtn.setChecked(true);
        } else {
            shippingNoRadioBtn.setChecked(true);
        }

        int tax_class_id = 0;
        int stock_status_id = 0;
        int length_class_id = 0;
        int weight_class_id = 0;
        if (taxDataArr != null) {
            taxClassData.clear();
            taxClassDataIds.clear();
            taxClassData.add("--- None ---");
            taxClassDataIds.add("0");

            for (int i = 0; i < taxDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(taxDataArr, i);
                taxClassData.add(generalFunc.getJsonValue("title", obj_temp));
                taxClassDataIds.add(generalFunc.getJsonValue("tax_class_id", obj_temp));

                if (generalFunc.getJsonValue("tax_class_id", productData).equalsIgnoreCase(generalFunc.getJsonValue("tax_class_id", obj_temp))) {
                    tax_class_id = i + 1;
                }

            }
        }

        if (stockStatusData != null) {
            outOfStockStatusData.clear();
            outOfStockStatusDataIds.clear();
            outOfStockStatusData.add("select status stock of data");
            for (int i = 0; i < stockStatusData.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(stockStatusData, i);
                outOfStockStatusData.add(generalFunc.getJsonValue("name", obj_temp));
                outOfStockStatusDataIds.add(generalFunc.getJsonValue("stock_status_id", obj_temp));
                if (generalFunc.getJsonValue("stock_status_id", productData).equalsIgnoreCase(generalFunc.getJsonValue("stock_status_id", obj_temp))) {
                    stock_status_id = i+1;
                }
            }
        }

        if (lengthClassDataArr != null) {

            lengthClassData.clear();
            lengthClassDataIds.clear();
            lengthClassData.add("select length");
            for (int i = 0; i < lengthClassDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(lengthClassDataArr, i);
                lengthClassData.add(generalFunc.getJsonValue("title", obj_temp));
                lengthClassDataIds.add(generalFunc.getJsonValue("length_class_id", obj_temp));

                if (generalFunc.getJsonValue("length_class_id", productData).equalsIgnoreCase(generalFunc.getJsonValue("length_class_id", obj_temp))) {
                    length_class_id = i+1;
                }
            }

        }
        if (weightClassDataArr != null) {

            weightClassData.clear();
            weightClassDataIds.clear();
            weightClassData.add("select weight of product");
            for (int i = 0; i < weightClassDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(weightClassDataArr, i);
                weightClassData.add(generalFunc.getJsonValue("title", obj_temp));
                weightClassDataIds.add(generalFunc.getJsonValue("weight_class_id", obj_temp));

                if (generalFunc.getJsonValue("weight_class_id", productData).equalsIgnoreCase(generalFunc.getJsonValue("weight_class_id", obj_temp))) {
                    weight_class_id = i+1;
                }
            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, taxClassData);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        taxClassSpinner.setAdapter(dataAdapter);

        if (tax_class_id != 0) {
            taxClassSpinner.setSelection(tax_class_id);
        }


        ArrayAdapter<String> weightClassAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, weightClassData);
        weightClassAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        weightClassSpinner.setAdapter(weightClassAdapter);
        weightClassSpinner.setSelection(weight_class_id);

        ArrayAdapter<String> lengthClassAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, lengthClassData);
        lengthClassAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lengthClassSpinner.setAdapter(lengthClassAdapter);
        lengthClassSpinner.setSelection(length_class_id);


        subtractStockData.clear();
        statusData.clear();
        subtractStockData.add("Select SubtractData");
        subtractStockData.add("Yes");
        subtractStockData.add("No");

        statusData.add("Select Product status");
        statusData.add("Enabled");
        statusData.add("Disabled");

        ArrayAdapter<String> statusAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, statusData);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(statusAdapter);

        if (generalFunc.getJsonValue("status", productData).equalsIgnoreCase("0")) {
            statusSpinner.setSelection(1);
        }

        ArrayAdapter<String> subtractStockAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, subtractStockData);
        subtractStockAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subtractStockSpinner.setAdapter(subtractStockAdapter);

        if (generalFunc.getJsonValue("subtract", productData).equalsIgnoreCase("0")) {
            subtractStockSpinner.setSelection(2);
        }
        else if (generalFunc.getJsonValue("subtract", productData).equalsIgnoreCase("1"))
            subtractStockSpinner.setSelection(1);
        ArrayAdapter<String> outOfStockStatusAdapter = new ArrayAdapter<>(getActContext(), R.layout.spinner_item, outOfStockStatusData);
        outOfStockStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        outOfStockStatusSpinner.setAdapter(outOfStockStatusAdapter);

        outOfStockStatusSpinner.setSelection(stock_status_id);


        //taxClassSpinner.setSelection(stock_status_id);

//        taxClassSpinner.setSelection(0);
    }

    public void generatePageError() {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                generateAlert.closeAlertBox();

                if (btn_id == 1) {
                    // manageProductAct.backImgView.performClick();
                }
            }
        });
        generateAlert.setContentMessage("", "Some error occurred. Please check your internet or try again later.");
        generateAlert.setPositiveBtn("OK");

        if (!manageProductAct.isFinishing()) {
            generateAlert.showAlertBox();
        }
    }
    public void updateProductData()
    {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "updateStoreProductData");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);


        if (!taxClassData.isEmpty())
            parameters.put("tax_class_id", taxClassDataIds.get(taxClassSpinner.getSelectedItemPosition()));
        parameters.put("subtract", subtractStockData.get(subtractStockSpinner.getSelectedItemPosition()).equalsIgnoreCase("Yes") ? "1" : "0");
        parameters.put("stock_status_id", outOfStockStatusDataIds.get(outOfStockStatusSpinner.getSelectedItemPosition()-1));
        parameters.put("shipping", shippingYesRadioBtn.isChecked() ? "1" : "0");
        //parameters.put("date_available", dateSelectTxtView.getText().toString());
        parameters.put("length", Utils.getText(lengthBox));
        parameters.put("width", Utils.getText(widthBox));
        parameters.put("height", Utils.getText(heightBox));
        parameters.put("length_class_id", lengthClassDataIds.get(lengthClassSpinner.getSelectedItemPosition()-1));
        parameters.put("weight", Utils.getText(weightBox));
        parameters.put("weight_class_id", weightClassDataIds.get(weightClassSpinner.getSelectedItemPosition()-1));
        parameters.put("status","1");// statusData.get(statusSpinner.getSelectedItemPosition()).equalsIgnoreCase("Enabled") ? "1" : "0");
        //  parameters.put("sort_order", Utils.getText(sortOrderBox));

        Log.d("data",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                //Log.e("Data Response","message "+responseString);
                //Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();

                if (responseString != null && !responseString.equals("")) {

                    //boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    /*if (isDataAvail == true) {
                        //getProductDetails();
                    }*/
                    //Log.e(">>> Error","error");
                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
}

