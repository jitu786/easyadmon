package e.admin.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import e.admin.Activity.Add_Product;
import e.admin.Activity.RelatedProductsActivity;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.helper.StartActProcess;

public class Link_Form extends Fragment
{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    GeneralFunctions generalFunc;
    TextView txtrelatedProduct;
    Button update;
    String productCategoryId="";
    LinearLayout relatedProductsContainerView,categoryContainerView;
    List<String> cat_arr=new ArrayList<>();
    List<String> relative_product=new ArrayList<>();
    ArrayList<String> manufacturerDataID = new ArrayList<>();
    ArrayList<String> manufacturerDataList = new ArrayList<>();
    String categoryIds = "";
    ArrayList<String> selectedFilterDataID = new ArrayList<>();
    ArrayList<String> selectedCategoryDataID = new ArrayList<>();
    ArrayList<String> selectedRelatedProductsID = new ArrayList<>();
    ArrayList<String> selectedDownloadDataID = new ArrayList<>();

    ArrayList<String> filterDataID = new ArrayList<>();
    ArrayList<String> filterDataList = new ArrayList<>();


    ArrayList<String> productTypeList = new ArrayList<>();

    ArrayList<String> downloadsDataID = new ArrayList<>();
    ArrayList<String> downloadsDataList = new ArrayList<>();

    ArrayList<String> relatedProductsDataID = new ArrayList<>();
    ArrayList<String> relatedProductsDataList = new ArrayList<>();

    Spinner spinner_cat,spinner_filter,spinner_rp;
    Add_Product manageProductAct;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    String[] category_arr_2 =new String[6];
    ArrayAdapter ab;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.link_form, container, false);
        generalFunc=new GeneralFunctions(getContext());
        spinner_cat=v.findViewById(R.id.spinner_cat);
        spinner_filter=v.findViewById(R.id.spinner_filter);
        manageProductAct=(Add_Product) getActivity();
        txtrelatedProduct=v.findViewById(R.id.txtrelatedProduct);
        update=v.findViewById(R.id.update);
        relatedProductsContainerView=v.findViewById(R.id.relatedProductsContainerView);
        categoryContainerView=v.findViewById(R.id.categoryContainerView);

        getcategory();
        getProductDetails();
        txtrelatedProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bn = new Bundle();
                bn.putString("product_id", manageProductAct.product_id);
                if (!categoryIds.equals("")) {
                    //Toast.makeText(manageProductAct, ""+categoryIds, Toast.LENGTH_SHORT).show();
                    bn.putString("productCategoryId", categoryIds);
                } else {
                    Toast.makeText(getActContext(), "Please Select the Category", Toast.LENGTH_SHORT).show();
                    return;
                }
                (new StartActProcess(getActContext())).startActForResult(getCurrentFragment(), RelatedProductsActivity.class, Utils.RELATED_PRODUCTS_REQ_CODE, bn);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateLinkData();
            }
        });
        return v;
    }
    public Fragment getCurrentFragment() {
        return this;
    }
    public void getcategory()
    {
        cat_arr.clear();
        cat_arr.add("Select category");
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("type", "getAllCategories");

        Log.e("category","cat"+parameters.toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(final String responseString) {
                Utils.printLog("ResponseData", "Data::" + responseString);
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);
                    if (isDataAvail)
                    {
                        JSONArray msgArr = generalFunc.getJsonArray(Utils.message_str, responseString);
                        dataList.clear();
                        if (msgArr != null)
                        {
                            for (int i = 0; i < msgArr.length(); i++) {

                                JSONObject obj_service = generalFunc.getJsonObject(msgArr, i);
                                cat_arr.add(generalFunc.getJsonValue("name", String.valueOf(obj_service)));
                                category_arr_2[i]= generalFunc.getJsonValue("name", obj_service);

                                HashMap<String, String> dataMap_products = new HashMap<>();
                                dataMap_products.put("name",generalFunc.getJsonValue("name", String.valueOf(obj_service)));
                                dataMap_products.put("category_id",generalFunc.getJsonValue("category_id", String.valueOf(obj_service)));
                                dataMap_products.put("image",generalFunc.getJsonValue("image", String.valueOf(obj_service)));
                                dataList.add(dataMap_products);
                            }

                            // Drop down layout style - list view with radio button
                            ab = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,cat_arr);
                            ab.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinner_cat.setAdapter(ab);
                            spinner_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    if(i>0) {
                                        categoryIds = dataList.get(i - 1).get("category_id");
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    }
                    else
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else
                {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    public void getProductDetails() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getStoreProductInfo");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);
        parameters.put("isLoadGeneralData", "Yes");

        Log.e("productInfo", "getProductDetails: " + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        displayInformation(responseString);
                    } else {
                        //  generatePageError();
//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    //generatePageError();
                }
            }
        });
        exeWebServer.execute();
    }

    private Context getActContext()
    {
        return getContext();
    }

    int manufacturer_id = 0;

    public void displayInformation(String responseString) {
        categoryContainerView.setVisibility(View.GONE);
        categoryContainerView.removeAllViews();
        relatedProductsContainerView.setVisibility(View.GONE);
        relatedProductsContainerView.removeAllViews();


        JSONObject productData = generalFunc.getJsonObject("ProductData", responseString);
//        String productTag = generalFunc.getJsonValue("ProductTag", responseString);
        JSONObject productDescriptionData = generalFunc.getJsonObject("ProductDescriptionData", responseString);

//        setProductTypeData();
        //      String productType = generalFunc.getJsonValue("product_type", productData);
        //    productTypeSpinner.setPrompt(productType);

        if (productData == null || productDescriptionData == null) {
//            generatePageError();
            return;
        }

        JSONArray manufacturerDataArr = generalFunc.getJsonArray("ManufacturerData", generalFunc.getJsonObject("GeneralData", responseString));
        JSONArray filterGroupDescDataArr = generalFunc.getJsonArray("FilterGroupDescData", generalFunc.getJsonObject("GeneralData", responseString));
        JSONArray filterDescDataArr = generalFunc.getJsonArray("FilterDescData", generalFunc.getJsonObject("GeneralData", responseString));
        JSONArray downloadDataArr = generalFunc.getJsonArray("DownloadData", generalFunc.getJsonObject("GeneralData", responseString));
        JSONArray productFilterDataArr = generalFunc.getJsonArray("ProductFilterData", responseString);
        JSONArray productCategoryDataArr = generalFunc.getJsonArray("ProductCategoryData", responseString);
        JSONArray productDownloadData = generalFunc.getJsonArray("ProductDownloadData", responseString);
        JSONArray productRelatedData = generalFunc.getJsonArray("ProductRelatedData", responseString);


        if (manufacturerDataArr != null) {
            manufacturerDataList.clear();
            manufacturerDataID.clear();
            for (int i = 0; i < manufacturerDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(manufacturerDataArr, i);
                manufacturerDataList.add(generalFunc.getJsonValue("name", obj_temp));
                manufacturerDataID.add(generalFunc.getJsonValue("manufacturer_id", obj_temp));

                if (generalFunc.getJsonValue("manufacturer_id", productData).equalsIgnoreCase(generalFunc.getJsonValue("manufacturer_id", obj_temp))) {
                    manufacturer_id = i;
                    // manufacturerAutoComplete.setText(manufacturerDataList.get(i));
                }
            }
        }

        if (productFilterDataArr != null) {
            selectedFilterDataID.clear();
            for (int i = 0; i < productFilterDataArr.length(); i++) {
                JSONObject temp_obj = generalFunc.getJsonObject(productFilterDataArr, i);
                String filter_id = generalFunc.getJsonValue("filter_id", temp_obj);
                selectedFilterDataID.add(filter_id);
            }
        }
        if (productRelatedData != null) {
            selectedRelatedProductsID.clear();
            for (int i = 0; i < productRelatedData.length(); i++) {
                JSONObject temp_obj = generalFunc.getJsonObject(productRelatedData, i);
                String product_id = generalFunc.getJsonValue("product_id", temp_obj);
                String PRODUCT_NAME = generalFunc.getJsonValue("PRODUCT_NAME", temp_obj);
                selectedRelatedProductsID.add(product_id);

                if (selectedRelatedProductsID.contains(product_id)) {
                    addSelectedProducts(product_id, Utils.html2text(PRODUCT_NAME), true);
                }
            }
        }

        if (productDownloadData != null) {
            selectedDownloadDataID.clear();
            for (int i = 0; i < productDownloadData.length(); i++) {
                JSONObject temp_obj = generalFunc.getJsonObject(productDownloadData, i);
                String download_id = generalFunc.getJsonValue("download_id", temp_obj);
                String DOWNLOAD_NAME = generalFunc.getJsonValue("DOWNLOAD_NAME", temp_obj);
                selectedDownloadDataID.add(download_id);

                Utils.printLog("DOWNLOAD_NAME", "::" + Utils.html2text(DOWNLOAD_NAME));
//                if (selectedDownloadDataID.contains(download_id)) {
//                    addSelectedDownload(download_id, Utils.html2text(DOWNLOAD_NAME), true);
//                }
            }
        }
        if (productCategoryDataArr.length() > 0) {
            selectedCategoryDataID.clear();
//            for (int i = 0; i < productCategoryDataArr.length(); i++) {
            for (int i = 0; i < 1; i++) {
                JSONObject temp_obj = generalFunc.getJsonObject(productCategoryDataArr, i);
                String category_id = generalFunc.getJsonValue("category_id", temp_obj);
                String CATEGORY_NAME = generalFunc.getJsonValue("CATEGORY_NAME", temp_obj);
                selectedCategoryDataID.add(category_id);

                Utils.printLog("CATEGORY_NAME", "::" + Utils.html2text(CATEGORY_NAME));
                if (selectedCategoryDataID.contains(category_id)) {
                    addSelectedCategory(category_id, Utils.html2text(CATEGORY_NAME), true);
                    setDefaultSpinner(category_id);

                }
            }
        }

        Utils.printLog("FIlterDATASELECTED", "::" + selectedFilterDataID.toString());

        if (filterGroupDescDataArr != null) {
            filterDataList.clear();
            filterDataID.clear();

            filterDataList.add("Select Filter");
            filterDataID.add("");

            ArrayList<String> addedFilterList = new ArrayList<>();
            for (int i = 0; i < filterGroupDescDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(filterGroupDescDataArr, i);
                String filter_group_id = generalFunc.getJsonValue("filter_group_id", obj_temp);
                String name_group = generalFunc.getJsonValue("name", obj_temp);

                for (int j = 0; j < filterDescDataArr.length(); j++) {
                    JSONObject obj_temp_1 = generalFunc.getJsonObject(filterDescDataArr, j);
                    String filter_group_id_1 = generalFunc.getJsonValue("filter_group_id", obj_temp_1);
                    String filter_id = generalFunc.getJsonValue("filter_id", obj_temp_1);
                    String name = generalFunc.getJsonValue("name", obj_temp_1);

                    if (filter_group_id_1.equalsIgnoreCase(filter_group_id)) {
                        filterDataList.add(name_group + " > " + name);
                        filterDataID.add(filter_id);
                    }

                    Utils.printLog("FIlterDATASELECTED", "::" + filter_id);
                    if (selectedFilterDataID.contains(filter_id) && !addedFilterList.contains(filter_id)) {
                        //addSelectedFilter(filter_id, name_group + " > " + name, true);
                        addedFilterList.add(filter_id);
                    }
                }
            }

        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, filterDataList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner_filter.setAdapter(dataAdapter);
        spinner_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (downloadDataArr != null) {
            downloadsDataList.clear();
            downloadsDataID.clear();

            downloadsDataList.add("Select Download");
            downloadsDataID.add("");

            for (int i = 0; i < downloadDataArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(downloadDataArr, i);
                String download_id = generalFunc.getJsonValue("download_id", obj_temp);
                String name = generalFunc.getJsonValue("name", obj_temp);
                downloadsDataList.add(name);
                downloadsDataID.add(download_id);
                if (selectedDownloadDataID.contains(download_id)) {
                    //addSelectedDownload(download_id, name, true);
                }
            }
        }

        relatedProductsDataID.clear();
        relatedProductsDataList.clear();

        relatedProductsDataID.add("");
        relatedProductsDataList.add("Select Related Products");
    }
    public void updateLinkData() {


        if (categoryIds.equals("")) {
            Toast.makeText(manageProductAct, "Please Select the Category", Toast.LENGTH_SHORT).show();
            return;
        }
        String filterIds = "";
        for (int i = 0; i < selectedFilterDataID.size(); i++) {
            if (!selectedFilterDataID.get(i).equals("")) {
                filterIds = filterIds.equals("") ? selectedFilterDataID.get(i) : filterIds + "," + selectedFilterDataID.get(i);
            }
        }
        String downloadsIds = "";
        for (int i = 0; i < selectedDownloadDataID.size(); i++) {
            if (!selectedDownloadDataID.get(i).equals("")) {
                downloadsIds = downloadsIds.equals("") ? selectedDownloadDataID.get(i) : downloadsIds + "," + selectedDownloadDataID.get(i);
            }
        }
        String relatedProductsIds = "";
        for (int i = 0; i < selectedRelatedProductsID.size(); i++) {
            if (!selectedRelatedProductsID.get(i).equals("")) {
                relatedProductsIds = relatedProductsIds.equals("") ? selectedRelatedProductsID.get(i) : relatedProductsIds + "," + selectedRelatedProductsID.get(i);
            }
        }
        Utils.printLog("downloadsIds", "::" + downloadsIds);

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "updateProductLink");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);
//        String manufactureID=getManufactureID(manufacturerAutoComplete);
//        parameters.put("manufacturerId", manufactureID);
        parameters.put("filterIds", filterIds);
//        if (productTypePosition > 0) {
//            parameters.put("product_type", productTypeList.get(productTypePosition));
//        }

        parameters.put("categoryIds", categoryIds);
        parameters.put("downloadsIds", downloadsIds);
        parameters.put("relatedProductsIds", relatedProductsIds);

        Log.e("datalink", "updateLinkData: " + parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        if (manageProductAct.product_id.equals("")) {
                            String product_id = generalFunc.getJsonValue("product_id", responseString);
                            manageProductAct.product_id = product_id;
                            manageProductAct.setResult(manageProductAct.RESULT_OK);
                        }
                        //setLabels();

                        getProductDetails();
                    }
                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.RELATED_PRODUCTS_REQ_CODE && data != null) {

            Utils.printLog("DATAINT", "::" + data.toString());
            txtrelatedProduct.setText(data.getStringExtra("productName"));
            addSelectedProducts(data.getStringExtra("product_id"), data.getStringExtra("productName"), false);
        }
    }
    public void addSelectedProducts(final String categoryId, String categoryName, boolean isAddView) {
        if (selectedRelatedProductsID.size() > 0 && isAddView == false) {

            for (int i = 0; i < selectedRelatedProductsID.size(); i++) {
                String tempCategoryId = selectedRelatedProductsID.get(i);

                if (tempCategoryId.equalsIgnoreCase(categoryId)) {
                    return;
                }
            }
        }
        LayoutInflater inflater = (LayoutInflater) getActContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View categoryView = inflater.inflate(R.layout.add_link_category_item, null);

        TextView categoryNameTxtView = (TextView) categoryView.findViewById(R.id.categoryNameTxtView);
        categoryNameTxtView.setText(categoryName);
        ImageView removeImgView = (ImageView) categoryView.findViewById(R.id.removeImgView);
        removeImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedRelatedProductsID.size() > 0) {

                    for (int i = 0; i < selectedRelatedProductsID.size(); i++) {
                        String tempCategoryId = selectedRelatedProductsID.get(i);
                        if (tempCategoryId.equalsIgnoreCase(categoryId)) {
                            selectedRelatedProductsID.remove(i);
                            categoryView.setVisibility(View.GONE);
                        }
                    }
                }

                if (selectedRelatedProductsID.size() == 0) {
                    relatedProductsContainerView.setVisibility(View.GONE);
                }
            }
        });
        if (isAddView == false) {
            selectedRelatedProductsID.add(categoryId);
        }
        relatedProductsContainerView.addView(categoryView);
        relatedProductsContainerView.setVisibility(View.VISIBLE);
    }
    public void addSelectedCategory(final String categoryId, String categoryName, boolean isAddView) {
        if (selectedCategoryDataID.size() > 0 && !isAddView) {

            for (int i = 0; i < selectedCategoryDataID.size(); i++) {
                String tempCategoryId = selectedCategoryDataID.get(i);

                if (tempCategoryId.equalsIgnoreCase(categoryId)) {
                    return;
                }
            }
        }
        LayoutInflater inflater = (LayoutInflater) getActContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View categoryView = inflater.inflate(R.layout.add_link_category_item, null);

        TextView categoryNameTxtView = (TextView) categoryView.findViewById(R.id.categoryNameTxtView);
        categoryNameTxtView.setText(categoryName);
        ImageView removeImgView = (ImageView) categoryView.findViewById(R.id.removeImgView);
        removeImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedCategoryDataID.size() > 0) {

                    for (int i = 0; i < selectedCategoryDataID.size(); i++) {
                        String tempCategoryId = selectedCategoryDataID.get(i);
                        if (tempCategoryId.equalsIgnoreCase(categoryId)) {
                            selectedCategoryDataID.remove(i);
                            categoryView.setVisibility(View.GONE);
                            productCategoryId = "";
                        }
                    }
                }


                if (selectedCategoryDataID.size() == 0) {
                    categoryContainerView.setVisibility(View.GONE);
                }
            }
        });
        if (isAddView) {
            selectedCategoryDataID.clear();
            selectedCategoryDataID.add(categoryId);
            productCategoryId = categoryId;

        }
        categoryContainerView.removeAllViews();
        categoryContainerView.addView(categoryView);
        categoryContainerView.setVisibility(View.VISIBLE);
    }

    private void setDefaultSpinner(String category_id)
    {
        int pos=0;
        if(category_id.equals("60"))
            pos=1;
        else if(category_id.equals("33"))
            pos=2;
        else if(category_id.equals("25"))
            pos=3;

        int spinnerPosition = ab.getPosition(category_arr_2[pos-1]);
        spinner_cat.setSelection(spinnerPosition);


    }


}