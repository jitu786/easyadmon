package e.admin.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import e.admin.Activity.Add_Product;
import e.admin.Activity.ProductDiscountAddActivity;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;
import e.admin.helper.CreateRoundedView;
import e.admin.helper.StartActProcess;
import e.admin.view.GenerateAlertBox;

public class Discount_Form extends Fragment
{
    View view;

    Add_Product manageProductAct;
    public GeneralFunctions generalFunc;

    //    MButton productInfoAddBtn;
    LinearLayout discountsContainerView;
    TextView noDiscountAvailTxtView;
    TextView addMoreImgTxtView;
    RadioButton rd_discount;
    RadioButton rd_special;
    boolean flag_spcial_dis=false;
    String p_price="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.discount_form, container, false);

        rd_discount=view.findViewById(R.id.rd_discount);
        rd_special=view.findViewById(R.id.rd_special);
        manageProductAct = (Add_Product) getActivity();
        generalFunc = manageProductAct.generalFunc;

        discountsContainerView = (LinearLayout) view.findViewById(R.id.discountsContainerView);
        noDiscountAvailTxtView = (TextView) view.findViewById(R.id.noDiscountAvailTxtView);

        addMoreImgTxtView = (TextView) view.findViewById(R.id.addMoreImgTxtView);


        addMoreImgTxtView.setOnClickListener(new setOnClickList());
        rd_special.setOnClickListener(new setOnClickList());
        rd_discount.setOnClickListener(new setOnClickList());

        new CreateRoundedView(getActContext().getResources().getColor(R.color.colorPrimaryDark), Utils.dipToPixels(getActContext(), 25), Utils.dipToPixels(getActContext(), 0), Color.parseColor("#DEDEDE"), addMoreImgTxtView);
        getProductDetails();
        return view;
    }




    public void getProductDetails() {
        discountsContainerView.removeAllViews();

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getStoreProductInfo");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", manageProductAct.product_id);

        Log.d("parameter",parameters.toString());
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        displayInformation(responseString);
                    } else {
                        generatePageError();
//                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generatePageError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void displayInformation(String responseString) {
        discountsContainerView.removeAllViews();

        JSONObject productData = generalFunc.getJsonObject("ProductData", responseString);
        HashMap<String, String> map = new HashMap<>();
        map.put("price", generalFunc.getJsonValue("price", productData).trim());
        p_price=generalFunc.getJsonValue("price", productData).trim();

        JSONArray productDiscountData = generalFunc.getJsonArray("ProductDiscountData", responseString);
        JSONArray ProductSpecialData = generalFunc.getJsonArray("ProductSpecialData", responseString);
//        String productTag = generalFunc.getJsonValue("ProductTag", responseString);
        JSONObject productDescriptionData = generalFunc.getJsonObject("ProductDescriptionData", responseString);

        if (productData == null || productDescriptionData == null) {
            generatePageError();
            return;
        }

        if (!flag_spcial_dis) {

            for (int i = 0; i < productDiscountData.length(); i++) {
                addDiscountView(generalFunc.getJsonObject(productDiscountData, i));
            }
            if (productDiscountData.length() > 0) {
                noDiscountAvailTxtView.setVisibility(View.GONE);
            } else {
                noDiscountAvailTxtView.setVisibility(View.VISIBLE);
            }
        }
        else if(flag_spcial_dis) {
            for (int i = 0; i < ProductSpecialData.length(); i++) {
                addDiscountView(generalFunc.getJsonObject(ProductSpecialData, i));
            }
            if (ProductSpecialData.length() > 0) {
                noDiscountAvailTxtView.setVisibility(View.GONE);
            } else {
                noDiscountAvailTxtView.setVisibility(View.VISIBLE);
            }
        }
        else
            noDiscountAvailTxtView.setVisibility(View.VISIBLE);

    }

    public void addDiscountView(final JSONObject obj_temp) {

        LayoutInflater inflater = (LayoutInflater) getActContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View categoryView = inflater.inflate(R.layout.item_discount_design, null);

        TextView headerTxtView = (TextView) categoryView.findViewById(R.id.headerTxtView);
        TextView dataTxtView = (TextView) categoryView.findViewById(R.id.dataTxtView);
        headerTxtView.setText("Group: " + generalFunc.getJsonValue("CUSTOMER_GROUP_NAME", obj_temp));

        dataTxtView.setText("Quantity: " + generalFunc.getJsonValue("quantity", obj_temp) + "\n" + "Priority: " + generalFunc.getJsonValue("priority", obj_temp) + "\n" + "Price: " + Utils.formatPrice(generalFunc.getJsonValue("price", obj_temp)) + "\n" + "Date Start: " + generalFunc.getJsonValue("date_start", obj_temp) + "\n" + "Date End: " + generalFunc.getJsonValue("date_end", obj_temp));
        ImageView removeImgView = (ImageView) categoryView.findViewById(R.id.removeImgView);
        removeImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(flag_spcial_dis)
                    confirmDeleteSDiscount(generalFunc.getJsonValue("product_special_id", obj_temp));
                else
                    confirmDeleteDiscount(generalFunc.getJsonValue("product_discount_id", obj_temp));
            }
        });

        new CreateRoundedView(Color.parseColor("#F2F2F2"), Utils.dipToPixels(getActContext(), 8), Utils.dipToPixels(getActContext(), 1), Color.parseColor("#DEDEDE"), categoryView);
        discountsContainerView.addView(categoryView);
        discountsContainerView.setVisibility(View.VISIBLE);
    }

    public void generatePageError() {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                generateAlert.closeAlertBox();

                if (btn_id == 1) {
                    //manageProductAct.backImgView.performClick();
                }
            }
        });
        generateAlert.setContentMessage("", "Some error occurred. Please check your internet or try again later.");
        generateAlert.setPositiveBtn("OK");

        generateAlert.showAlertBox();
    }

    public void confirmDeleteDiscount(final String product_discount_id) {
        final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
        generateAlert.setCancelable(false);
        generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
                generateAlert.closeAlertBox();

                if (btn_id == 1) {
                    deleteDiscount(product_discount_id);
                }
            }
        });
        generateAlert.setContentMessage("", "Do you want to delete the discount?");
        generateAlert.setPositiveBtn("OK");
        generateAlert.setNegativeBtn("Cancel");

        generateAlert.showAlertBox();
    }
   public void confirmDeleteSDiscount(final String product_discount_id) {
       final GenerateAlertBox generateAlert = new GenerateAlertBox(getActContext());
       generateAlert.setCancelable(false);
       generateAlert.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
           @Override
           public void handleBtnClick(int btn_id) {
               generateAlert.closeAlertBox();

               if (btn_id == 1) {
                   deleteDiscount(product_discount_id);
               }
           }
       });
        generateAlert.setContentMessage("", "Do you want to delete the special product?");
       generateAlert.setPositiveBtn("OK");
       generateAlert.setNegativeBtn("Cancel");
       generateAlert.showAlertBox();
   }
    public void deleteDiscount(String product_discount_id) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        if(flag_spcial_dis)
        {
            parameters.put("type", "deleteProductSpecial");
            parameters.put("customer_id", generalFunc.getMemberId());
            parameters.put("product_id", manageProductAct.product_id);
            parameters.put("product_special_id", product_discount_id);
        }
        else {
            parameters.put("type", "deleteProductDiscount");
            parameters.put("customer_id", generalFunc.getMemberId());
            parameters.put("product_id", manageProductAct.product_id);
            parameters.put("product_discount_id", product_discount_id);
        }
        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        getProductDetails();
                    }
                    generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {
        return manageProductAct.getActContext();
    }

    public Fragment getCurrentFragment() {
        return this;
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            if (view.getId() == addMoreImgTxtView.getId()) {
                Bundle bn = new Bundle();
                bn.putString("product_id", manageProductAct.product_id);
                bn.putString("PAGE_MODE", "DISCOUNT");
                bn.putString("p_price", p_price);

                (new StartActProcess(getActContext())).startActForResult(getCurrentFragment(), ProductDiscountAddActivity.class, Utils.ADD_DISCOUNT_REQ_CODE, bn);
            }
            else if(view.getId()==rd_discount.getId())
            {
                checkRadio();
            }
            else if(view.getId()==rd_special.getId())
                checkRadio();
        }
    }

    public void checkData() {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.ADD_DISCOUNT_REQ_CODE && resultCode == manageProductAct.RESULT_OK) {
            getProductDetails();
        }
    }

    private void checkRadio() {
        if(rd_discount.isChecked()) {

            flag_spcial_dis=false;
            getProductDetails();
        }
        else {
            flag_spcial_dis=true;
            getProductDetails();
        }

    }

}



