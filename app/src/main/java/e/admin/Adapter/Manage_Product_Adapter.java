package e.admin.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Activity.Add_Product;
import e.admin.Activity.Manage_Product;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

public class Manage_Product_Adapter extends RecyclerView.Adapter<Manage_Product_Adapter.Myview>
{
    Context context;
    ArrayList<HashMap<String, String>> dataList;
    /*
        OnItemClickListener mItemClickListener;
    */
    GeneralFunctions generalFunc;

    public Manage_Product_Adapter(Context context, ArrayList<HashMap<String, String>> dataList)
    {
        this.context = context;
        this.dataList=dataList;
    }

    public void filterList(ArrayList<HashMap<String, String>> dataList1) {
        this.dataList=dataList1;
        notifyDataSetChanged();
    }

    /*  public interface OnItemClickListener {
          void onItemClickList(View v, int position, int btnType, int qty);
      }
      public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
          this.mItemClickListener = mItemClickListener;
      }*/
    @NonNull
    @Override
    public Manage_Product_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.store_product_list, viewGroup, false);
        return new Manage_Product_Adapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Manage_Product_Adapter.Myview holder, final int position) {
        HashMap<String,String> map=dataList.get(position);
        generalFunc=new GeneralFunctions(context);

        holder.p_name.setText(map.get("name"));
        holder.price.setText("$"+map.get("price"));

        Glide
                .with(context)
                .load(map.get("image"))
                .placeholder(R.drawable.imageicon)
                .into(holder.img);
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog1 = new Dialog(context);
                ImageView cancle,done;
                dialog1.setContentView(R.layout.delete_dialog);
                cancle=dialog1.findViewById(R.id.cancle);
                done=dialog1.findViewById(R.id.done);
                Window window1 = dialog1.getWindow();
                WindowManager.LayoutParams wlp1 = window1.getAttributes();
                window1.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                wlp1.width = WindowManager.LayoutParams.MATCH_PARENT;
                wlp1.gravity = Gravity.CENTER_VERTICAL;
                window1.setAttributes(wlp1);
                cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                    }
                });
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                        //mItemClickListener.onItemClickList(view,position,1,0);
                        deleteStoreProduct(dataList.get(position).get("product_id"));

                    }
                });


                dialog1.show();

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mItemClickListener.onItemClickList(view,position,2,0);
                Intent intent=new Intent(context, Add_Product.class);
                intent.putExtra("product_id",dataList.get(position).get("product_id"));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        CardView edit,delete;
        ImageView img;
        TextView p_name,price;
        public Myview(@NonNull View itemView) {
            super(itemView);
            edit=itemView.findViewById(R.id.edit);
            delete=itemView.findViewById(R.id.delete);

            img=itemView.findViewById(R.id.img);
            p_name=itemView.findViewById(R.id.p_name);
            price=itemView.findViewById(R.id.price);
        }
    }
    public void deleteStoreProduct(String product_id) {
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "deleteStoreProduct");
        parameters.put("customer_id", generalFunc.getMemberId());
        parameters.put("product_id", product_id);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setLoaderConfig(context, true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(Utils.action_str, responseString);

                    if (isDataAvail == true) {
                        //findProducts();
                        //generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                        Toast.makeText(context,"Your product is deleted successfully.",Toast.LENGTH_SHORT).show();
                        Intent i2 = new Intent(context, Manage_Product.class);
                        context.startActivity(i2);
                        ((Activity)context).finish();
                    } else {
                        generalFunc.showGeneralMessage("", generalFunc.getJsonValue(Utils.message_str, responseString));
                    }

                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }
}

