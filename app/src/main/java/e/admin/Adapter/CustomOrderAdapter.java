package e.admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Activity.ChangeOrderStatus;
import e.admin.Activity.CustomOrder;
import e.admin.R;
import e.admin.generalfiles.Utils;

public class CustomOrderAdapter extends RecyclerView.Adapter<CustomOrderAdapter.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList;
    /*
        private CustomOrderAdapter.OnItemClickListener mItemClickListener;
    */
    public CustomOrderAdapter(Context context, ArrayList<HashMap<String, String>> dataList)

    {
        this.context = context;
        this.dataList = dataList;

    }

   /* public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType);
    }
    public void setOnItemClickListener(CustomOrderAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }*/


    @NonNull
    @Override
    public CustomOrderAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_custom_order, viewGroup, false);
        return new CustomOrderAdapter.Myview(v);

    }

    public void filterList(ArrayList<HashMap<String, String>> dataList)
    {
        this.dataList=dataList;
        notifyDataSetChanged();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomOrderAdapter.Myview holder, final int position) {
        final HashMap<String, String> item = dataList.get(position);

        holder.order_id.setText(item.get("order_id"));
        holder.p_name.setText(item.get("product_name"));
        holder.u_name.setText(item.get("firstname")+" "+item.get("lastname"));
        holder.email.setText(item.get("email"));
        holder.telephone.setText(item.get("telephone"));
        holder.city.setText(item.get("shipping_city"));
        holder.paymenttype.setText(item.get("payment_method"));
        //holder.price.setText(" Rs. "+item.get("total"));
        holder.price.setText(Utils.formatPrice(item.get("total")));
        holder.status.setText(item.get("status_name"));


        holder.change_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //mItemClickListener.onItemClickList(v,position,1);
                Intent int_edit_address = new Intent(context, ChangeOrderStatus.class);
                int_edit_address.putExtra("oderDATA", dataList.get(position));
                context.startActivity(int_edit_address);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {

        TextView order_id,p_name,u_name,email,telephone,city,paymenttype,price,status;
        TextView change_status;
        public Myview(@NonNull View itemView) {
            super(itemView);
            change_status=itemView.findViewById(R.id.change_status);
            order_id=itemView.findViewById(R.id.order_id);
            p_name=itemView.findViewById(R.id.p_name);
            u_name=itemView.findViewById(R.id.u_name);
            email=itemView.findViewById(R.id.email);
            telephone=itemView.findViewById(R.id.telephone);
            city=itemView.findViewById(R.id.city);
            paymenttype=itemView.findViewById(R.id.paymenttype);
            price=itemView.findViewById(R.id.price);
            status=itemView.findViewById(R.id.status);

        }
    }
}