package e.admin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.R;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

public class ProductImagesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<HashMap<String, String>> list;
    Context mContext;
    public GeneralFunctions generalFunc;

    private OnItemClickListener mItemClickListener;

    public ProductImagesRecyclerAdapter(Context mContext, ArrayList<HashMap<String, String>> list, GeneralFunctions generalFunc, boolean isFooterEnabled) {
        this.mContext = mContext;
        this.list = list;
        this.generalFunc = generalFunc;

    }

    public interface OnItemClickListener {
        void onItemClickList(View v, int btn_type, int position);
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_image_list, parent, false);
        return new ViewHolder(view);


    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder1, final int position) {

        ViewHolder holder= (ViewHolder) holder1;
        final HashMap<String, String> item = list.get(position);
        //final RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) holder;



        Glide
                .with(mContext)
                .load(item.get("imageURL"))
                .placeholder(R.drawable.watch)
                //.error(R.drawable.load)
                .into(holder.productImgView);


        holder.productImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.contentArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClickList(view, -1, position);
                }
            }
        });
        holder.deleteImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.printLog("Delete", "ImgTapped");
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClickList(view, 1, position);
                }
            }
        });

    }



    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView productImgView;
        public ImageView deleteImgView;
        public View contentArea;
        public CardView defaultText;

        public ViewHolder(View view) {
            super(view);
            contentArea = view;
            productImgView = (ImageView) view.findViewById(R.id.productImgView);
            deleteImgView = (ImageView) view.findViewById(R.id.deleteImgView);
            defaultText = view.findViewById(R.id.defaultText);
        }
    }




    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {

        return list.size();


    }



}


