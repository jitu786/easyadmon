package e.admin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import e.admin.R;

public class SelectCollectionPointAdapter extends RecyclerView.Adapter<SelectCollectionPointAdapter.Myview>
{


    Context context;
    // ArrayList<HashMap<String, String>> dataList;

    public SelectCollectionPointAdapter(Context context/*, ArrayList<HashMap<String, String>> dataList*/)

    {
        this.context = context;
        //this.dataList = dataList;

    }


    @NonNull
    @Override
    public SelectCollectionPointAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.select_cp_list_dialog_adapter, viewGroup, false);
        return new SelectCollectionPointAdapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final SelectCollectionPointAdapter.Myview holder, int position) {


    }

    @Override
    public int getItemCount() {
        return 5;
    }


    public class Myview extends RecyclerView.ViewHolder {

        public Myview(@NonNull View itemView) {
            super(itemView);

        }
    }
}


