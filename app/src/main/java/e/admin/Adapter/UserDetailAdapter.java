package e.admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import e.admin.Activity.AddCollectionPoint;
import e.admin.R;

public class UserDetailAdapter  extends RecyclerView.Adapter<UserDetailAdapter.Myview>
{


    Context context;
    // ArrayList<HashMap<String, String>> dataList;

    public UserDetailAdapter(Context context/*, ArrayList<HashMap<String, String>> dataList*/)

    {
        this.context = context;
        //this.dataList = dataList;

    }


    @NonNull
    @Override
    public UserDetailAdapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_user_detail, viewGroup, false);
        return new UserDetailAdapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final UserDetailAdapter.Myview holder, int position) {


        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2 = new Intent(context, AddCollectionPoint.class);
                context.startActivity(i2);

            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }


    public class Myview extends RecyclerView.ViewHolder {

        CardView edit;
        public Myview(@NonNull View itemView) {
            super(itemView);
            edit=itemView.findViewById(R.id.edit);


        }
    }
}

