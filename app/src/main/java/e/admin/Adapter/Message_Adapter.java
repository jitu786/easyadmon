package e.admin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.HashMap;

import e.admin.R;

public class Message_Adapter extends RecyclerView.Adapter<Message_Adapter.Myview>
{


    Context context;
   // ArrayList<HashMap<String, String>> dataList;

    public Message_Adapter(Context context/*, ArrayList<HashMap<String, String>> dataList*/)

    {
        this.context = context;
        //this.dataList = dataList;

    }


    @NonNull
    @Override
    public Message_Adapter.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.message_list, viewGroup, false);
        return new Message_Adapter.Myview(v);

    }

    @Override
    public void onBindViewHolder(@NonNull final Message_Adapter.Myview holder, int position) {
       /* final HashMap<String, String> item = dataList.get(position);
        holder.title.setText(item.get("title"));
        holder.description.setText(item.get("message"));
        holder.date.setText(item.get("date"));
*/

    }

    @Override
    public int getItemCount() {
        return 5;
    }


    public class Myview extends RecyclerView.ViewHolder {

        TextView title,description,date;
        public Myview(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            title=itemView.findViewById(R.id.title);
            description=itemView.findViewById(R.id.description);

        }
    }
}


