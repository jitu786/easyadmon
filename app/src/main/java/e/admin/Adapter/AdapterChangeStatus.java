package e.admin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import e.admin.R;

public class AdapterChangeStatus extends RecyclerView.Adapter<AdapterChangeStatus.Myview>
{


    Context context;
    AdapterChangeStatus.OnItemClickListener mItemClickListener;


    ArrayList<HashMap<String, String>> dataList1;

    List<String> arrayList;
    public AdapterChangeStatus(Context context,/*, ArrayList<HashMap<String, String>> dataList1*/ArrayList<HashMap<String, String>> dataList1, List<String> service_id_list)

    {
        this.context = context;
        this.dataList1=dataList1;
        this.arrayList=service_id_list;

    }

    @NonNull
    @Override
    public AdapterChangeStatus.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_change_order_status, viewGroup, false);
        return new AdapterChangeStatus.Myview(v);

    }

    public void setData(List<String> service_id_list)
    {
        this.arrayList=service_id_list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType,String catname);
    }

    public void setOnItemClickListener(AdapterChangeStatus.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(@NonNull final AdapterChangeStatus.Myview holder, final int position) {

        final HashMap<String, String> item = dataList1.get(position);
        holder.checkBox.setText(item.get("name"));

      /*  if(arrayList.contains(item.get("order_status_id"))) {
            holder.checkBox.setChecked(true);
        }
        else {

            holder.checkBox.setChecked(false);

        }*/

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                if(b)
                    mItemClickListener.onItemClickList(holder.checkBox.getRootView(),position,1,"1");
                else
                    mItemClickListener.onItemClickList(holder.checkBox.getRootView(),position,1,"0");
            }
        });
    }
    @Override
    public int getItemCount() {
        return dataList1.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        public Myview(@NonNull View itemView) {
            super(itemView);
            checkBox=itemView.findViewById(R.id.checkBox);
        }
    }
}

