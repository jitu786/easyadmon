package e.admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Activity.ChangeOrderStatus;
import e.admin.Activity.ChangeReturnOrderStatus;
import e.admin.R;

public class CustomReturnOrder  extends RecyclerView.Adapter<CustomReturnOrder.Myview>
{


    Context context;
    ArrayList<HashMap<String, String>> dataList;
    /*
        private CustomOrderAdapter.OnItemClickListener mItemClickListener;
    */
    public CustomReturnOrder(Context context, ArrayList<HashMap<String, String>> dataList)

    {
        this.context = context;
        this.dataList = dataList;

    }

   /* public interface OnItemClickListener {
        void onItemClickList(View v, int position, int btnType);
    }
    public void setOnItemClickListener(CustomOrderAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }*/


    @NonNull
    @Override
    public CustomReturnOrder.Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_custom_return_order, viewGroup, false);
        return new CustomReturnOrder.Myview(v);

    }

    public void filterList(ArrayList<HashMap<String, String>> dataList)
    {
        this.dataList=dataList;
        notifyDataSetChanged();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomReturnOrder.Myview holder, final int position) {
        final HashMap<String, String> item = dataList.get(position);


        holder.order_id.setText(item.get("order_id"));
        holder.return_id.setText(item.get("return_id"));
        holder.p_name.setText(item.get("product_name"));
        holder.u_name.setText(item.get("firstname")+" "+item.get("lastname"));
        holder.email.setText(item.get("email"));
        holder.telephone.setText(item.get("telephone"));
       // holder.city.setText(item.get("shipping_city"));
        holder.status.setText(item.get("return_status_name"));
        holder.reson_name.setText(item.get("reason_name"));

        if (item.get("action_name").equals("") && item.get("action_name").isEmpty())
        {
            holder.action_txt.setText("Waiting");
        }
        else {
            holder.action_txt.setText(item.get("action_name"));
        }

        holder.change_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //mItemClickListener.onItemClickList(v,position,1);
                Intent int_edit_address = new Intent(context, ChangeReturnOrderStatus.class);
                int_edit_address.putExtra("oderDATA", dataList.get(position));
                context.startActivity(int_edit_address);
            }
        });



    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class Myview extends RecyclerView.ViewHolder {

        TextView order_id,return_id,p_name,u_name,email,telephone,city,reson_name,status,action_txt;
        TextView change_status;
        public Myview(@NonNull View itemView) {
            super(itemView);
            change_status=itemView.findViewById(R.id.change_status);
            order_id=itemView.findViewById(R.id.order_id);
            return_id=itemView.findViewById(R.id.return_id);
            p_name=itemView.findViewById(R.id.p_name);
            u_name=itemView.findViewById(R.id.u_name);
            email=itemView.findViewById(R.id.email);
            telephone=itemView.findViewById(R.id.telephone);
            city=itemView.findViewById(R.id.city);
            reson_name=itemView.findViewById(R.id.reson_name);
            status=itemView.findViewById(R.id.status);
            action_txt=itemView.findViewById(R.id.action_txt);


        }
    }
}



