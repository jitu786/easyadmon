package e.admin.Service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import e.admin.R;

/**
 * Created by Imran on 11-04-2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String title,message;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Intent intent;
        title = "E-Commerce App";
        message = "";

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this , "Default");
        builder.setSmallIcon(R.drawable.not);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        builder.setLargeIcon(icon);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setColor(getResources().getColor(R.color.colorPrimary));
        }
        builder.setContentTitle(title);

        message = remoteMessage.getNotification().getBody();
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(message);

//        setMessage(title,message);
        builder.setStyle(bigTextStyle);
        builder.setAutoCancel(true);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Default",
                    "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }

        String click_action = remoteMessage.getNotification().getClickAction();

        if (click_action.equals("")) {
            click_action = "Ecommerce_buyer_order_activity";
        }


        intent = new Intent(click_action);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 100, intent, PendingIntent.FLAG_ONE_SHOT);
        builder.setContentText(message);
        builder.setContentIntent(pendingIntent);

        Random random = new Random();
        manager.notify(random.nextInt(), builder.build());

    }
//
//    private void setMessage(String title, String message) {
//        HashMap<String, String> param=new HashMap<>();
//        param.put("type","saveMessage");
////        param.put("user_id",getMemberId());
//        param.put("title", title);
//        param.put("message", message);
//        GeneralFunctions generalFunc=new GeneralFunctions(this);
//        param.put("date",generalFunc.getStringFromDate(Calendar.getInstance()));
//
//        Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
//
//        ExecuteWebServerUrl url=new ExecuteWebServerUrl(param);
//        url.execute();
//    }
/*
    public static void checkNotification(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_cart);
        builder.setContentTitle("FireBase Notification");
        builder.setContentText("Test");
        builder.setContentIntent(pendingIntent);

        GeneralFunctions generalFunctions = new GeneralFunctions(context);
        generalFunctions.addMessageToDB("Title", "Message");
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            manager.notify(0, builder.build());
        }
    }*/
}
