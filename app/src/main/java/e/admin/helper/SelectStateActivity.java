package e.admin.helper;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import e.admin.Adapter.PinnedSectionListAdapter;
import e.admin.Model.CountryListItem;
import e.admin.Model.PinnedSectionListView;
import e.admin.R;
import e.admin.generalfiles.ExecuteWebServerUrl;
import e.admin.generalfiles.GeneralFunctions;
import e.admin.generalfiles.Utils;

public class SelectStateActivity extends AppCompatActivity implements PinnedSectionListAdapter.CountryClick {

    ArrayList<CountryListItem> items_list;

    private static final String TAG = "SelectStateActivity";
    TextView titleTxt;

    ImageView backImgView;

    GeneralFunctions generalFunc;

    ProgressBar loading;

    TextView noResTxt;

    PinnedSectionListView country_list;
    PinnedSectionListAdapter pinnedSectionListAdapter;

    private CountryListItem[] sections;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_state);

        generalFunc = new GeneralFunctions(getActContext());


        titleTxt = findViewById(R.id.titleTxt);
        backImgView = findViewById(R.id.backImgView);
        noResTxt = findViewById(R.id.noResTxt);
        loading = findViewById(R.id.loading);

        country_list = findViewById(R.id.country_list);


        country_list.setShadowVisible(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            country_list.setFastScrollEnabled(true);
            country_list.setFastScrollAlwaysVisible(true);
        }
        items_list = new ArrayList<>();


        setLabels();

        backImgView.setOnClickListener(new setOnClickList());
        getCountryList();
    }

    @Override
    public void countryClickList(CountryListItem countryListItem) {
        Bundle bn = new Bundle();
        bn.putString("zone_id", countryListItem.getZone_id());
        bn.putString("zone_name", countryListItem.text);
        bn.putString("region_id", countryListItem.regionId);
//        bn.putString("vCountryCode", countryListItem.getvCountryCode());
        bn.putString("zone_code", countryListItem.getvPhoneCode());
        new StartActProcess(getActContext()).setOkResult(bn);
        finish();
    }

    public Context getActContext() {
        return SelectStateActivity.this;
    }

    public void setLabels() {
        if (getIntent().hasExtra("type"))
            titleTxt.setText("Select Region");
        else {
            titleTxt.setText("Select City");
        }
    }

    public void getCountryList() {

        if (loading.getVisibility() != View.VISIBLE) {
            loading.setVisibility(View.VISIBLE);
        }

        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("type", "stateList");


        String region_id = "";

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            region_id = bundle.getString("region_id","");
        }

        parameters.put("country_id", getIntent().getStringExtra("iCountryId"));
        parameters.put("region_id", region_id);

        noResTxt.setVisibility(View.GONE);
        Log.e(TAG, "setResponse: "+ parameters.toString() );
        // Log.e(TAG, "setResponse: " +region_id);

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(parameters);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                noResTxt.setVisibility(View.GONE);


                Utils.printLog("StateResponse", ":" + responseString);
                if (responseString != null && !responseString.equals("")) {

                    closeLoader();

                    if (GeneralFunctions.checkDataAvail(Utils.action_str, responseString)) {

                        if (getIntent().hasExtra("type")) {
                            regionList(responseString);
                        } else {
                            cityList(responseString);
                        }

                    } else {
                        noResTxt.setText("Error occurred");
                        noResTxt.setVisibility(View.VISIBLE);
                    }

                } else {
                    generateErrorView();
                }
            }
        });
        exeWebServer.execute();
    }

    private void regionList(String responseString) {
        items_list.clear();

        sections = new CountryListItem[generalFunc.parseInt(0, generalFunc.getJsonValue("totalValues", responseString))];
        pinnedSectionListAdapter = new PinnedSectionListAdapter(getActContext(), items_list, sections);
        country_list.setAdapter(pinnedSectionListAdapter);
        pinnedSectionListAdapter.setCountryClickListener(SelectStateActivity.this);
        items_list.clear();
        pinnedSectionListAdapter.notifyDataSetChanged();

        JSONArray countryArr = generalFunc.getJsonArray("StateList", responseString);

        if (countryArr != null) {
            int sectionPosition = 0, listPosition = 0;
            for (int i = 0; i < countryArr.length(); i++) {
                JSONObject tempJson = generalFunc.getJsonObject(countryArr, i);

                String key_str = generalFunc.getJsonValue("key", tempJson.toString());
                String count_str = generalFunc.getJsonValue("TotalCount", tempJson.toString());

                CountryListItem section = new CountryListItem(CountryListItem.SECTION, key_str);
                section.sectionPosition = sectionPosition;
                section.listPosition = listPosition++;
                section.CountSubItems = generalFunc.parseInt(0, count_str);
                onSectionAdded(section, sectionPosition);
                items_list.add(section);

                JSONArray subListArr = generalFunc.getJsonArray("List", tempJson.toString());

                for (int j = 0; j < subListArr.length(); j++) {
                    JSONObject subTempJson = generalFunc.getJsonObject(subListArr, j);

                    CountryListItem countryListItem = new CountryListItem(CountryListItem.ITEM, generalFunc.getJsonValue("name", subTempJson.toString()));
                    countryListItem.sectionPosition = sectionPosition;
                    countryListItem.listPosition = listPosition++;
                    countryListItem.setvPhoneCode(generalFunc.getJsonValue("code", ""));
                    countryListItem.setZone_id(generalFunc.getJsonValue("zone_id", subTempJson.toString()));
                    countryListItem.setRegionId(generalFunc.getJsonValue("region_id", subTempJson.toString()));
                    items_list.add(countryListItem);
                }

                sectionPosition++;
            }
        }
        if (countryArr == null || countryArr.length() < 1) {

            noResTxt.setText("No data available");
            noResTxt.setVisibility(View.VISIBLE);
        }
        pinnedSectionListAdapter.notifyDataSetChanged();

    }

    private void cityList(String responseString) {
        items_list.clear();

        sections = new CountryListItem[generalFunc.parseInt(0, generalFunc.getJsonValue("totalValues", responseString))];
        pinnedSectionListAdapter = new PinnedSectionListAdapter(getActContext(), items_list, sections);
        country_list.setAdapter(pinnedSectionListAdapter);

        pinnedSectionListAdapter.setCountryClickListener(SelectStateActivity.this);
        items_list.clear();
        pinnedSectionListAdapter.notifyDataSetChanged();

        JSONArray countryArr = generalFunc.getJsonArray("StateList", responseString);

        if (countryArr != null) {
            int sectionPosition = 0, listPosition = 0;
            for (int i = 0; i < countryArr.length(); i++) {
                JSONObject tempJson = generalFunc.getJsonObject(countryArr, i);

                String key_str = generalFunc.getJsonValue("key", tempJson.toString());
                String count_str = generalFunc.getJsonValue("TotalCount", tempJson.toString());

                CountryListItem section = new CountryListItem(CountryListItem.SECTION, key_str);
                section.sectionPosition = sectionPosition;
                section.listPosition = listPosition++;
                section.CountSubItems = generalFunc.parseInt(0, count_str);
                onSectionAdded(section, sectionPosition);
                items_list.add(section);

                JSONArray subListArr = generalFunc.getJsonArray("List", tempJson.toString());

                for (int j = 0; j < subListArr.length(); j++) {
                    JSONObject subTempJson = generalFunc.getJsonObject(subListArr, j);

                    CountryListItem countryListItem = new CountryListItem(CountryListItem.ITEM, generalFunc.getJsonValue("name", subTempJson.toString()));
                    countryListItem.sectionPosition = sectionPosition;
                    countryListItem.listPosition = listPosition++;
                    countryListItem.setvPhoneCode(generalFunc.getJsonValue("code", subTempJson.toString()));
                    countryListItem.setiCountryId(generalFunc.getJsonValue("zone_id", subTempJson.toString()));
                    items_list.add(countryListItem);
                }

                sectionPosition++;
            }
        }

        if (countryArr == null || countryArr.length() < 1) {

            noResTxt.setText("No data available");
            noResTxt.setVisibility(View.VISIBLE);
        }
        pinnedSectionListAdapter.notifyDataSetChanged();

    }


    public void closeLoader() {
        if (loading.getVisibility() == View.VISIBLE) {
            loading.setVisibility(View.GONE);
        }
    }

    public void generateErrorView() {

        closeLoader();

    }

    protected void onSectionAdded(CountryListItem section, int sectionPosition) {
        sections[sectionPosition] = section;
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.backImgView:
                    SelectStateActivity.super.onBackPressed();
                    break;
            }
        }
    }
}
