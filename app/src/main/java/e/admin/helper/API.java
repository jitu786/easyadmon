package e.admin.helper;



import e.admin.Model.RegisterResponse;
import e.admin.Model.ResponseSpinner;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {

    String BASE_URL ="http://bntbatouta.esy.es/test/mob_api/api/";
    @GET("myitemuk_api.php?")
    Call<RegisterResponse> Register(
            @Query("email_id") String email_id,
            @Query("password") String password
    );
    @GET("webservices.php?")
    Call<ResponseSpinner> getSpinnerData(@Query("type") String type,
                                         @Query("option_id") String option_id) ;

}