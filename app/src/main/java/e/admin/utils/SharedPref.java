package e.admin.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref {
    public static void WriteSharePrefrence(Context context, String key, String value)
    {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }
 public static void WriteSharePrefrence1(Context context, String key, ArrayList<HashMap<String, String>> value)
    {
        try {


            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            Gson gson = new Gson();
            String json = gson.toJson(value);
            editor.putString(key, json);
            editor.commit();
        }catch (Exception e){}
    }
    public static ArrayList<HashMap<String, String>> ReadSharePrefrence1(Context context, String key)
    {
        String data;
        Gson gson = new Gson();
        ArrayList<HashMap<String, String>> items = new ArrayList<>();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getString(key, "");

        Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
        items = gson.fromJson(data, type);

        return items;
    }




    public static String ReadSharePrefrence(Context context, String key)
    {
        String data;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getString(key, "");
        editor.apply();
        return data;
    }

    public static void ClearaSharePrefrence(Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().clear().apply();
    }
    public static void savePostdata(Context context, HashMap<String, String> dataMap_products) {
        //convert to string using gson
        Gson gson = new Gson();
        String hashMapString = gson.toJson(dataMap_products);
        //save in shared prefs
        SharedPreferences prefs =context.getSharedPreferences("postdata", MODE_PRIVATE);
        prefs.edit().putString("hashString", hashMapString).apply();
    }
    public static HashMap<String,String> getPostData(Context context)
    {
        Gson gson = new Gson();
        SharedPreferences prefs =context.getSharedPreferences("postdata", MODE_PRIVATE);
        String storedHashMapString = prefs.getString("hashString", "");
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        HashMap<String, String> testHashMap2 = gson.fromJson(storedHashMapString, type);
        return testHashMap2;
    }
}
