package e.admin.Model;

public class ProductCategoriesData {


    /* Initialisation */

    private String id = null;
    private String product_id = "";
    private String size = "";
    private String color = "";
    private String price = "";
    private String option_value_name = "";
    private String price_prefix = "";
    private String option_name = "";
    private String product_option_id = "";
    private String product_option_value_id = "";
    private String option_value_id="";

    public String getOption_value_id() {
        return option_value_id;
    }

    public String getProduct_option_value_id() {
        return product_option_value_id;
    }

    public void setProduct_option_value_id(String product_option_value_id) {
        this.product_option_value_id = product_option_value_id;
    }

    public String getProduct_option_id() {
        return product_option_id;
    }

    public void setProduct_option_id(String product_option_id) {
        this.product_option_id = product_option_id;
    }

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public String getPrice_prefix() {
        return price_prefix;
    }

    public void setPrice_prefix(String price_prefix) {
        this.price_prefix = price_prefix;
    }

    public String getOption_value_name() {
        return option_value_name;
    }

    public void setOption_value_name(String option_value_name) {
        this.option_value_name = option_value_name;
    }
    /* Setter */

    public void setId(String id) {
        if (!id.equals("")) this.id = id;
    }

    public void setProduct_id(String product_id) {
        if (!product_id.equals("")) this.product_id = product_id;
    }

    public void setSize(String size) {
        if (!size.equals("")) this.size = size;
    }

    public void setColor(String color) {
        if (!color.equals("")) this.color = color;
    }

    public void setPrice(String price) {
        if (!price.equals("")) this.price = price;
    }

    /* Get table name */
    public static String getTableName() {
        return "product_categories_data";
    }

    /* Getter */

    public String getId() {
        return id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getPrice() {
        return price;
    }

    /* Table Name String */

    public class s {
        public static final String id = "id";
        public static final String product_id = "product_id";
        public static final String size = "size";
        public static final String color = "color";
        public static final String price = "price";
    }

}

