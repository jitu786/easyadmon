package e.admin.Model;

public class ResponseSpinner {
    String Action;
    Message message;

    public String getAction() {
        return Action;
    }

    public Message getMessage() {
        return message;
    }

    public class Message
    {
        Data[] OptionValueDescData;
        public class Data
        {
            String option_value_id;
            String language_id;
            String option_id;
            String name;

            public String getOption_value_id() {
                return option_value_id;
            }

            public String getLanguage_id() {
                return language_id;
            }

            public String getOption_id() {
                return option_id;
            }

            public String getName() {
                return name;
            }
        }


        public Data[] getOptionValueDescData() {
            return OptionValueDescData;
        }
    }
}

