package e.admin.bannerslider.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import e.admin.bannerslider.banners.Banner;
import e.admin.bannerslider.banners.DrawableBanner;
import e.admin.bannerslider.events.OnBannerClickListener;
import e.admin.bannerslider.views.AdjustableImageView;


/**
 * @author S.Shahini
 * @since 11/23/16
 */

public class BannerFragment extends Fragment {
    private Banner banner;

    public BannerFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        banner = getArguments().getParcelable("banner");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (banner != null) {

            final AdjustableImageView imageView = new AdjustableImageView(getContext());
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(banner.getScaleType());
            if (banner instanceof DrawableBanner) {
              /*  DrawableBanner drawableBanner = (DrawableBanner) banner;
                Picasso.with(getContext()).load(drawableBanner.getDrawable()).fit().centerInside().into(imageView);
            } else {
                final RemoteBanner remoteBanner = (RemoteBanner) banner;
                if (remoteBanner.getErrorDrawable() == null && remoteBanner.getPlaceHolder() == null) {
                   try {
                       Picasso.with(getActivity()).load(remoteBanner.getUrl()).fit().centerInside().into(imageView);
                   }catch (Exception e ){
                       Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                   }

                } else {
                    if (remoteBanner.getPlaceHolder() != null && remoteBanner.getErrorDrawable() != null) {
                        Picasso.with(getActivity()).load(remoteBanner.getUrl()).fit().centerInside().placeholder(remoteBanner.getPlaceHolder()).error(remoteBanner.getErrorDrawable()).into(imageView);
                    } else if (remoteBanner.getErrorDrawable() != null) {
                        Picasso.with(getActivity()).load(remoteBanner.getUrl()).fit().centerInside().error(remoteBanner.getErrorDrawable()).into(imageView);
                    } else if (remoteBanner.getPlaceHolder() != null) {
                        Picasso.with(getActivity()).load(remoteBanner.getUrl()).fit().centerInside().placeholder(remoteBanner.getPlaceHolder()).into(imageView);
                    }
                }
            */}
            imageView.setOnTouchListener(banner.getOnTouchListener());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OnBannerClickListener onBannerClickListener = banner.getOnBannerClickListener();
                    if (onBannerClickListener != null) {
                        onBannerClickListener.onClick(banner.getPosition());
                    }
                }
            });

            return imageView;
        } else {
            throw new RuntimeException("banner cannot be null");
        }
    }

    public static BannerFragment newInstance(Banner banner) {
        Bundle args = new Bundle();
        args.putParcelable("banner", banner);
        BannerFragment fragment = new BannerFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
