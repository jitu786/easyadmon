package e.admin.generalfiles;

import android.annotation.SuppressLint;
import android.util.Base64;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class MyCrypt {

    public static final String key_id = "shivInfo1234";
    public static String key = "";



    public static String decrypt_data(String encData)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.DECRYPT_MODE, skeySpec);

        byte[] original = cipher
                .doFinal(Base64.decode(encData.getBytes(), Base64.DEFAULT));
        return new String(original).trim();
    }

    public static String encrypt_data(String data) {

        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("ddHH");
        df.setTimeZone(TimeZone.getTimeZone("GMT+2"));
        String strDate = df.format(date);

        key=key_id+strDate;


        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = null;
        byte[] original;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

            original = Base64.encode(cipher.doFinal(data.getBytes()), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


        return new String(original);
    }
}
