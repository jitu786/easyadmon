package e.admin.generalfiles;


import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Build;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import e.admin.view.MyProgressDialog;

/**
 * Created by Shroff on 7/3/2016.
 */
public class Utils {

    public static final int GOOGLE_SIGN_IN_REQ_CODE = 786;
    public static final int SELECT_COUNTRY_REQ_CODE = 123;
    public static final int ImageUpload_MINIMUM_WIDTH = 256;
    public static final int ImageUpload_MINIMUM_HEIGHT = 256;
    public static final int SELECT_STATE_REQ_CODE =108 ;
    public static final String TempProfileImageName = "temp_pic_img.png";
    public static final String errorImageQuality="Image quality is low. Please select another image.";
    public static final int PAYMENT_CODE = 2321;
    public static HashMap<String, String> sellerFirstEntryData = new HashMap<>();
    public static boolean isSellerFirstEntry = false;
    public static boolean isSellerDetailsOK = false;
    public static boolean isSellerInfoOK = false;
    public static boolean isSellerBankDetailsOK = false;
    public static final int SELECT_CITY_REQ_CODE =40 ;
    public static final int ADD_ADDRESS_REQ_CODE = 1234;
    public static final int RELATED_PRODUCTS_REQ_CODE =9876 ;
    public static final int ADD_DISCOUNT_REQ_CODE = 9898;
    public static final int ADD_CATEGORIES_REQ_CODE = 9899;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    public static ArrayList<String> currencySymbolList = new ArrayList<>();
    public static List<String> countryList = new ArrayList<>();

    public static final String IndiaCode = "99";
    public static final String UkCode = "222";
    public static final String MalawiCode = "128";
    public static final String TanzaniaCode = "208";
    public static String numberAM = "";
    public static final String countryIndia = "India";
    public static final String countryUK = "United Kingdom";
    public static final String countryMalawi = "Malawi";
    public static final String countryTanzania = "Tanzania";

    public static int minPasswordLength = 2;
    public static String isFirstLaunchFinished = "isFirstLaunchFinished";
    public static String iMemberId_KEY = "iMemberId";
    public static String userLoggedIn_key = "isUserLoggedIn";
    public static String countryToDisplay = "Malawi";

    public static String action_str = "Action";
    public static String message_str = "message";
    public static String firebaseToken;
    public static String defaultImage = "defaultImage";

    static MyProgressDialog myPDialog;
    //Single Instance object
    private static Utils instance = null;
    public static String symbol_left = "";
    public static String symbol_right = "";
    public static ArrayList<String> currencyList = new ArrayList<>();
    public static List<HashMap<String, String>> orderStatusList = new ArrayList<>();
    public static String currency = "";
    //
    private Utils() {
    }

    //Single Instance get
    public static Utils getInstance() {
        if (instance == null)
            instance = new Utils();

        return instance;
    }
    public static String formatPrice(String price) {
        return "Rs." + price;
       /* if (currency.equals("")) {
            currency = getCurrency(countryToDisplay);
        }
        return currency.trim() + getPriceValue(price, currency);*/
    }
    public static String getCurrency(String country) {
        switch (country){
            case countryMalawi:
                return "$ ";
            case countryUK:
                return "$ ";
            case countryTanzania:
                return "$ ";
            default:
                return "$ ";
        }
    }
    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

    public static String getPriceValue(String price,String currency) {
        if (price.equals("")) {
            return "";
        }
        Double newPrice;
        Double priceValue = Double.parseDouble(price.replace(",", ""));
        if (!currency.equals(Utils.getCurrency(UkCode))) {
            Double ratio = getCurrencyValue(currency);
            if (ratio != null) {
                newPrice = priceValue * ratio;
            } else newPrice = priceValue;
        } else {
            newPrice = priceValue;
        }
        return String.format(java.util.Locale.US, "%.2f", newPrice);
       /*else {
            return String.valueOf(newPrice);
        }*/
    }
    public static boolean isValidImageResolution(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path, options);
        int width = options.outWidth;
        int height = options.outHeight;

        return width >= Utils.ImageUpload_MINIMUM_WIDTH && height >= Utils.ImageUpload_MINIMUM_HEIGHT;
    }

    private static Double getCurrencyValue(String currency) {
        int len=currencySymbolList.size();
        for(int i=0;i<len;i++){
            if (currencySymbolList.get(i).trim().equals(currency.trim())) {
                return Double.parseDouble(currencyList.get(i));
            }
        }
        return null;
    }
    public static void printLog(String tag, String msg) {
        Log.d(tag, msg);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static String[] generateImageParams(String key, String content) {
        String[] tempArr = new String[2];
        tempArr[0] = key;
        tempArr[1] = content;

        return tempArr;
    }
    public static void hideKeyboard(Activity act) {

        if (act.getCurrentFocus() == null) {
            return;
        }

        InputMethodManager inputManager = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static int dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics));
    }

    public static boolean setErrorFields(EditText editBox, String error) {
        editBox.setError(error);
        return false;
    }

    public static void removeInput(EditText editBox) {
        editBox.setInputType(InputType.TYPE_NULL);
        editBox.setFocusableInTouchMode(false);
        editBox.setFocusable(false);

        editBox.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                return true;
            }
        });
    }
   /* public static String html2text(String html) {
      return Jsoup.parse(html).text();
    }*/

    public static int generateViewId() {
        /**
         * Generate a value suitable for use in {@link #setId(int)}.
         * This value will not collide with ID values generated at build time by aapt for R.id.
         *
         * @return a generated ID value
         */

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            for (; ; ) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) {
                    return result;
                }
            }

        } else {
            return View.generateViewId();
        }

    }
    public static String getText(EditText editBox) {
        return editBox.getText().toString();
    }
    public static void runGC() {
        System.gc();
    }


    public static void closeLoader() {
        if (myPDialog != null) {
            myPDialog.close();
            myPDialog = null;
        }
    }

    public static MyProgressDialog showLoader(Context mContext) {
        myPDialog = new MyProgressDialog(mContext, true, "Loading");
        myPDialog.show();

        return myPDialog;
    }

    public static int getExifRotation(String path) {
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        return orientation;

    }


    public static boolean checkText(EditText editBox) {
        return !getText(editBox).trim().equals("");
    }

    private static boolean isValid(Double getRatio) {
        return getRatio != null;
    }

}