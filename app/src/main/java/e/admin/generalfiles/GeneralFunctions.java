package e.admin.generalfiles;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;


import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import e.admin.view.GenerateAlertBox;

public class GeneralFunctions {
    Context mContext;

    public static final int MY_PERMISSIONS_REQUEST = 51;

    public GeneralFunctions(Context mContext) {
        this.mContext = mContext;
    }
    public void storeUserData(String memberId) {
        storedata(Utils.iMemberId_KEY, memberId);
        storedata(Utils.userLoggedIn_key, "1");
    }
    public void storedata(String key, String data) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(key, data);
        editor.commit();
    }
    public String capFully(String string) {
        return WordUtils.capitalizeFully(string.toLowerCase().trim());
    }
    public static void printHashKey(Context mContext) {
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Utils.printLog("hashKey", ":" + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Utils.printLog("hashKey", ":Exception:" + e.getMessage());
        } catch (Exception e) {
            Utils.printLog("hashKey", ":Exception:" + e.getMessage());
        }
    }

    public GenerateAlertBox getConfirmDialog() {
        GenerateAlertBox generateAlert = new GenerateAlertBox(mContext);
        generateAlert.setContentMessage("", "Are you want to become seller ?");
        generateAlert.setPositiveBtn("Yes");
        generateAlert.setNegativeBtn("No");
        generateAlert.showAlertBox();
        return generateAlert;
    }
    public static Integer parseInt(int orig, String value) {

        try {
            int value_int = Integer.parseInt(value);
            return value_int;
        } catch (Exception e) {
            return orig;
        }

    }
    public boolean isUserLoggedIn()
    {
        return !retriveValue(Utils.userLoggedIn_key).equals("") && retriveValue(Utils.userLoggedIn_key).equals("1");
    }

   /* public void logOUTFrmFB() {
        LoginManager.getInstance().logOut();
    }
   */ public void ErrorOption() {
        GenerateAlertBox generateAlert = new GenerateAlertBox(mContext);
        generateAlert.setContentMessage("Error Occurred", "Please product option select..");
        generateAlert.setPositiveBtn("OK");
        generateAlert.showAlertBox();
    }
    public float parseFloat(float orig, String value) {

        try {
            float value_int = Float.parseFloat(value);
            return value_int;
        } catch (Exception e) {
            return orig;
        }


    }
    public String retriveValue(String key) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String value_str = mPrefs.getString(key, "");

        return value_str;
    }
    public static boolean checkDataAvail(String key, String response) {
        try {
            JSONObject obj_temp = new JSONObject(response);

            String action_str = obj_temp.getString(key);

            //Log.e(">>> KEY ",action_str);

            if (!action_str.equals("") && !action_str.equals("0") && action_str.equals("1")) {
                return true;
            }

        } catch (JSONException e) {
            e.printStackTrace();

            return false;
        }

        return false;
    }

    public GenerateAlertBox showGeneralMessage(String title, String message) {
        GenerateAlertBox generateAlert = new GenerateAlertBox(mContext);
        generateAlert.setContentMessage(title, message);
        generateAlert.setPositiveBtn("OK");
        generateAlert.showAlertBox();
        return generateAlert;
    }

    public String getJsonValue(String key, String response) {

        try {
            JSONObject obj_temp = new JSONObject(response);

            String value_str = obj_temp.getString(key);

            if (value_str != null && !value_str.equals("null") && !value_str.equals("")) {
                return value_str;
            }

        } catch (JSONException e) {
            e.printStackTrace();

            return "";
        }

        return "";
    }


    public void showError() {
        GenerateAlertBox generateAlert = new GenerateAlertBox(mContext);
        generateAlert.setContentMessage("Error Occurred", "Please try again later.");
        generateAlert.setPositiveBtn("OK");
        generateAlert.showAlertBox();
    }


    public String getMemberId() {
        if (isUserLoggedIn() == true) {
            return retriveValue(Utils.iMemberId_KEY);
        } else {
            return "";
        }
    }
    public String getJsonValue(String key, JSONObject obj) {
        try {
            String value_str = obj.getString(key);
            if (value_str != null && !value_str.equals("null") && !value_str.equals("")) {
                return value_str;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return "";
    }
    public JSONObject getJsonObject(String key, String responseString) {
        try {
            JSONObject obj_temp = new JSONObject(responseString);
            JSONObject obj = obj_temp.getJSONObject(key);
            return obj;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }

    }
    public JSONArray getJsonArray(String key, String response) {
        try {
            JSONObject obj_temp = new JSONObject(response);
            JSONArray obj_temp_arr = obj_temp.getJSONArray(key);

            return obj_temp_arr;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }
    public JSONArray getJsonArray(String key, JSONObject obj) {
        try {
            JSONArray obj_temp_arr = obj.getJSONArray(key);

            return obj_temp_arr;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }
    public JSONObject getJsonObject(JSONArray arr, int position) {
        try {
            JSONObject obj_temp = arr.getJSONObject(position);

            return obj_temp;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }

    }
    public void startActivity(Class activityClass, Bundle bundle) {
        Intent intent = new Intent(mContext, activityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    public void startActivity(Class activityClass) {
        startActivity(activityClass, new Bundle());
    }


}
